var express = require("express")
var router = express.Router()

var ctrl = require("../controllers/assistants_controller")

router.post("/get",function (req,res,next){
    ctrl.get(req,res,next)
})

router.post("/save",function (req,res,next){
    ctrl.save(req,res,next)
})

module.exports = router
var express = require("express")
var router = express.Router()

var ctrl = require("../controllers/transactions_controller")

//#region BRANCHES

router.post("/get-pending-transactions",function (req,res,next){
    ctrl.getPendingTransactions(req,res,next)
})

router.post("/update-pending-transaction",function (req,res,next){
    ctrl.updatePendingTransaction(req,res,next)
})

router.post("/update-schedule",function (req,res,next){
    ctrl.updateSchedule(req,res,next)
})

router.post("/get-pending-payments",function (req,res,next){
    ctrl.getPendingPayments(req,res,next)
})

router.post("/add-payment",function (req,res,next){
    ctrl.addPayment(req,res,next)
})

module.exports = router
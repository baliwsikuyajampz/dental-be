var express = require("express")
var router = express.Router()

var controller = require("../controllers/expenses_controller")

//#region BRANCHES

router.post("/get-categories",function (req,res,next){
    controller.getCategories(req,res,next)
})

router.post("/save-categories",function (req,res,next){
    controller.saveCategories(req,res,next)
})

router.post("/get-categories-list",function (req,res,next){
    controller.getCategoriesList(req,res,next)
})

router.post("/get",function (req,res,next){
    controller.get(req,res,next)
})

router.post("/save",function (req,res,next){
    controller.save(req,res,next)
})

router.post("/delete-record",function (req,res,next){
    controller.deleteRecord(req,res,next)
})


module.exports = router
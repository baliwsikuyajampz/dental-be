var express = require("express")
var router = express.Router()

var user_ctrl = require("../controllers/users_controller")

router.post("/get-users",function (req,res,next){
    user_ctrl.getUsers(req,res,next)
})

router.post("/auth",function (req,res,next){
    user_ctrl.auth(req,res,next)
})

router.post("/change-password",function (req,res,next){
    user_ctrl.changePass(req,res,next)
})

router.post("/get-roles",function (req,res,next){
    user_ctrl.getRoles(req,res,next)
})

router.post("/save-user",function (req,res,next){
    user_ctrl.saveUser(req,res,next)
})

router.post("/add-user",function (req,res,next){
    user_ctrl.addUser(req,res,next)
})

router.post("/update-user",function (req,res,next){
    user_ctrl.updateUser(req,res,next)
})

router.post("/delete-user",function (req,res,next){
    user_ctrl.deleteUser(req,res,next)
})

router.post("/get-user-by-id-logs",function (req,res,next){
    user_ctrl.getUserByIdLogs(req,res,next)
})



module.exports = router
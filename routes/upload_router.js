var express = require('express')
var router = express.Router()
var upload = require('../services/upload_service')
var upload_attachment = require('../services/upload_attachment_service')

router.post('/upload-file', function (req, res, next) {
    upload.uploadFile(req, res, next)
})

router.post('/upload-attachment', function (req, res, next) {
    upload_attachment.uploadFile(req, res, next)
})

module.exports = router
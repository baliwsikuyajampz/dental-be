var express = require("express")
var router = express.Router()

var ctrl = require("../controllers/dashboard_controller")

router.post("/get-daily-sales",function (req,res,next){
    ctrl.getDailySales(req,res,next)
})

router.post("/get-daily-expenses",function (req,res,next){
    ctrl.getDailyExpenses(req,res,next)
})


router.post("/get-monthly-sales",function (req,res,next){
    ctrl.getMonthlySales(req,res,next)
})

router.post("/get-pending-clients",function (req,res,next){
    ctrl.getPendingClients(req,res,next)
})

router.post("/get-pending-patients-list",function (req,res,next){
    ctrl.getPendingPatientsList(req,res,next)
})

router.post("/get-total-unpaid-amount",function (req,res,next){
    ctrl.getTotalUnpaidAmount(req,res,next)
})

router.post("/get-grand-total",function (req,res,next){
    ctrl.getGrandTotal(req,res,next)
})



module.exports = router
var express = require("express")
var router = express.Router()

var doctor_ctrl = require("../controllers/doctors_controller")

//#region BRANCHES

router.post("/get",function (req,res,next){
    doctor_ctrl.get(req,res,next)
})

router.post("/save",function (req,res,next){
    doctor_ctrl.save(req,res,next)
})

router.post("/get-doctor-commission-by-id",function (req,res,next){
    doctor_ctrl.getDoctorCommissionById(req,res,next)
})

router.post("/get-doctor-details-by-id",function (req,res,next){
    doctor_ctrl.getDoctorDetailsById(req,res,next)
})


module.exports = router
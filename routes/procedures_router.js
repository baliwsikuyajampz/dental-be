var express = require("express")
var router = express.Router()

var procedures_controller = require("../controllers/procedures_controller")

//#region BRANCHES

router.post("/get",function (req,res,next){
    procedures_controller.get(req,res,next)
})

router.post("/save",function (req,res,next){
    procedures_controller.save(req,res,next)
})

router.post("/category/save",function (req,res,next){
    procedures_controller.categorySave(req,res,next)
})

router.post("/category/get",function (req,res,next){
    procedures_controller.categoryGet(req,res,next)
})

module.exports = router
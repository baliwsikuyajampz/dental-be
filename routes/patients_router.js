var express = require("express")
var router = express.Router()

var patients_ctrl = require("../controllers/patients_controller")

//#region BRANCHES

router.post("/get",function (req,res,next){
    patients_ctrl.get(req,res,next)
})

router.post("/save",function (req,res,next){
    patients_ctrl.save(req,res,next)
})

router.post("/get-patient-by-id",function (req,res,next){
    patients_ctrl.getPatientById(req,res,next)
})

router.post("/get-patient-info-by-id",function (req,res,next){
    patients_ctrl.getPatientInfoById(req,res,next)
})

router.post("/search-patient-by-name",function (req,res,next){
    patients_ctrl.searchPatientByName(req,res,next)
})

router.post("/save_upload_attachment",function (req,res,next){
    patients_ctrl.saveUploadAttachment(req,res,next)
})

router.post("/get_upload_attachment",function (req,res,next){
    patients_ctrl.getUploadAttachment(req,res,next)
})

router.post("/save_upload_profile",function (req,res,next){
    patients_ctrl.saveUploadProfile(req,res,next)
})

router.post("/update-patient-about",function (req,res,next){
    patients_ctrl.updatePatientAbout(req,res,next)
})

router.post("/get-patient-transactions-by-id",function (req,res,next){
    patients_ctrl.getPatientTransactionsById(req,res,next)
})

router.post("/delete-patient",function (req,res,next){
    patients_ctrl.deletePatient(req,res,next)
})



module.exports = router
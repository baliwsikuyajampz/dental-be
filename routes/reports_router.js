var express = require("express")
var router = express.Router()

var ctrl = require("../controllers/reports_controller")

//#region BRANCHES

router.post("/get-profit-and-loss",function (req,res,next){
    ctrl.getProfitAndLoss(req,res,next)
})

router.post("/get-commissions",function (req,res,next){
    ctrl.getCommissions(req,res,next)
})

router.post("/test",function (req,res,next){
    ctrl.test(req,res,next)
})

router.post("/get-assistants-commissions",function (req,res,next){
    ctrl.getAssistantsCommissions(req,res,next)
})

router.post("/get-secretary-commissions",function (req,res,next){
    ctrl.getSecretaryCommissions(req,res,next)
})

router.post("/get-daily-sales",function (req,res,next){
    ctrl.getDailySales(req,res,next)
})

router.post("/get-montly-sales",function (req,res,next){
    ctrl.getMonthlySales(req,res,next)
})

router.post("/get-annual-sales",function (req,res,next){
    ctrl.getAnnualSales(req,res,next)
})

router.post("/get-commissions-search",function (req,res,next){
    ctrl.getCommissionsSearch(req,res,next)
})

router.post("/get-assistants-commissions-search",function (req,res,next){
    ctrl.getAssistantsCommissionsSearch(req,res,next)
})

router.post("/get-secretary-commissions-search",function (req,res,next){
    ctrl.getSecretaryCommissionsSearch(req,res,next)
})

router.post("/get-daily-sales-details-by-page-change",function (req,res,next){
    ctrl.getDailySalesDetailsByPageChange(req,res,next)
})

router.post("/get-monthly-sales-details-by-page-change",function (req,res,next){
    ctrl.getMonthlySalesDetailsByPageChange(req,res,next)
})

router.post("/get-annual-sales-details-by-page-change",function (req,res,next){
    ctrl.getAnnualSalesDetailsByPageChange(req,res,next)
})

module.exports = router
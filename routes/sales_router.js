var express = require("express")
var router = express.Router()

var sales_controller = require("../controllers/sales_controller")

router.post("/get",function (req,res,next){
    sales_controller.get(req,res,next)
})

router.post("/save",function (req,res,next){
    sales_controller.save(req,res,next)
})

router.post("/void-transaction",function (req,res,next){
    sales_controller.voidTransaction(req,res,next)
})

router.post("/get-sales",function (req,res,next){
    sales_controller.getSales(req,res,next)
})


module.exports = router
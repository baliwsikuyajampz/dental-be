var express = require("express")
var router = express.Router()

var ctrl = require("../controllers/secretary_controller")

router.post("/save",function (req,res,next){
    ctrl.save(req,res,next)
})

router.post("/get",function (req,res,next){
    ctrl.get(req,res,next)
})


module.exports = router
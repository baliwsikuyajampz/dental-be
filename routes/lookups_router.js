var express = require("express")
var router = express.Router()

var lookups_controller = require("../controllers/lookups_controller")

//#region BRANCHES

router.post("/get-patients",function (req,res,next){
    lookups_controller.getPatients(req,res,next)
})

router.post("/get-doctors",function (req,res,next){
    lookups_controller.getDoctors(req,res,next)
})

router.post("/get-procedures",function (req,res,next){
    lookups_controller.getProcedures(req,res,next)
})

router.post("/get-assistants",function (req,res,next){
    lookups_controller.getAssistants(req,res,next)
})

router.post("/get-branch-lookup",function (req,res,next){
    lookups_controller.getBranchLookup(req,res,next)
})

router.post("/get-procedure-parents",function (req,res,next){
    lookups_controller.getProcedureParents(req,res,next)
})

router.post("/get-secretaries",function (req,res,next){
    lookups_controller.getSecretaries(req,res,next)
})

router.post("/get-expense-categories",function (req,res,next){
    lookups_controller.getExpenseCategories(req,res,next)
})

router.post("/get-procedures-ranks",function (req,res,next){
    lookups_controller.getProceduresRanks(req,res,next)
})

router.post("/get-ranks",function (req,res,next){
    lookups_controller.getRanks(req,res,next)
})


router.post("/get-mode-of-payments",function (req,res,next){
    lookups_controller.getModeOfPayments(req,res,next)
})




module.exports = router
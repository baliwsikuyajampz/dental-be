const express       = require('express');
const cors          = require('cors');
const config        = require('./config/config')
var app             = express();
const http          = require("http")
const server        = http.createServer(app)
const { Server }    = require("socket.io")

const cors_options  = config.CorsOptions
const env_settings  = config.EnvSettings

const io            = new Server(server, { cors: cors_options })
const path          = require('path');

const user_router     = require("./routes/users_router")
const upload_router   = require("./routes/upload_router")
const settings_router = require("./routes/settings_router")
const patients_router = require("./routes/patients_router")
const doctor_router  = require("./routes/doctors_router")
const lookups_router  = require("./routes/lookups_router")
const procedures_router = require("./routes/procedures_router")
const sales_router = require("./routes/sales_router")
const expenses_router = require("./routes/expenses_router")
const reports_router = require("./routes/reports_router")
const dashboard_router = require("./routes/dashboard_router")
const assistants_router = require("./routes/assistants_router")
const secretary_router = require("./routes/secretary_router")
const transactions_router = require("./routes/transactions_router")
const ranks_router = require("./routes/ranks_router")

app.use(cors());
app.use(
    express.urlencoded({
      extended: true,
    })
  );
  
app.use(express.json({limit:"100mb"})); 
app.use(express.urlencoded({limit:"100mb",extended:true})); 

app.use(express.static(
  path.join(__dirname, '/_file_uploads/patients')))

app.use("/users",user_router)
app.use("/uploads",upload_router)
app.use("/settings",settings_router)
app.use("/patients",patients_router)
app.use("/doctors",doctor_router)
app.use("/lookups",lookups_router)
app.use("/procedures",procedures_router)
app.use("/sales",sales_router)
app.use("/expenses",expenses_router)
app.use("/reports",reports_router)
app.use("/dashboard",dashboard_router)
app.use("/assistants",assistants_router)
app.use("/secretary",secretary_router)
app.use("/transactions",transactions_router)
app.use("/ranks",ranks_router)



server.listen(env_settings.Port, () => console.log("Express server is running at port no : " + env_settings.Port))
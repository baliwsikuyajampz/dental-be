var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

//#region BRANCHES

const getPatients = async function(req){
    let e = req.body

    let qry = "SELECT id,fname,mname,lname,date_created,CONCAT(lname,', ',fname,' ',mname) as fullname FROM `patients_tbl` WHERE is_active = '1' ORDER BY lname,fname,mname"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            date_created    : item.date_created
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getDoctors = async function(req){
    let e = req.body

    let qry = "SELECT a.id,a.`branch_id`,b.`branch_name`,a.fname,a.mname,a.lname,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname FROM `dentists_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.branch_id WHERE a.is_active = '1' ORDER BY lname,fname,mname"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_name     : item.branch_name,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            date_created    : item.date_created
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getProcedures = async function(req){
    let e = req.body

    let qry = "SELECT a.id,b.`procedure_name` AS category,a.`procedure_name`,a.`procedure_desc`,a.`price`,a.`commission_type`,a.`commission_rate`,a.assistants_rate,a.assistants_type,a.secretary_rate,a.secretary_type,a.isDentistAllowed,a.isAssistantAllowed,a.isSecretaryAllowed,a.haveQuantity,a.quantityComsThreshold,a.greaterThanQty,a.lessThanQty FROM `procedures_tbl` a INNER JOIN procedures_tbl b ON b.id = a.parent_id WHERE a.`parent_id` <> 0"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                       : item.id,
            category                 : item.category,
            procedure_name           : item.procedure_name,
            dentist                  : null,
            assistants               : [],
            secretaries              : [],
            price                    : 0,
            commission_type          : item.commission_type,
            commission_rate          : item.commission_rate,
            assistants_rate          : item.assistants_rate,
            assistants_type          : item.assistants_type,
            secretary_rate           : item.secretary_rate,
            secretary_type           : item.secretary_type,
            isDentistAllowed            : item.isDentistAllowed,
            isAssistantAllowed          : item.isAssistantAllowed,
            isSecretaryAllowed          : item.isSecretaryAllowed,
            quantity                    : 1,
            quantityComsThreshold       : 0,
            haveQuantity                : item.haveQuantity,
            greaterThanQty              : item.greaterThanQty,
            lessThanQty                : item.lessThanQty,
            proc_status                 : 'done',
            procedure_quantity_req   : await getProcedureQtyReqTbl(item.id),
            date                     : moment(item.date).format("YYYY-MM-DD"),
            time                     : moment(item.date).format("HH:MM"),
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getProcedureQtyReqTbl = async function(id){
    let qry = "SELECT id,procedure_id,qty,dentist_rate,assistant_rate,secretary_rate FROM `procedures_qty_req_tbl` WHERE procedure_id = ?"
    const resQry = await conn.getQuery(qry,[id]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                       : item.id,
            procedure_id                 : item.procedure_id,
            qty                         : item.qty,
            dentist_rate                 : item.dentist_rate,
            assistant_rate                 : item.assistant_rate,
            secretary_rate                 : item.secretary_rate,
            date                     : moment(item.date).format("YYYY-MM-DD")
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getAssistants = async function(req){
    let e = req.body

    let qry = "SELECT a.id,a.`branch_id`,a.fname,a.mname,a.lname,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname,b.`branch_name` FROM `assistants_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.`branch_id` WHERE a.is_active = '1' ORDER BY a.lname,a.fname,a.mname"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_id       : item.branch_id,
            branch_name       : item.branch_name,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            date_created    : item.date_created
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getBranchLookup = async function(req){
    let e = req.body

    let qry = "SELECT * FROM `branches_lookup_tbl` WHERE is_active = '1'" 

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_code     : item.branch_code,
            branch_name     : item.branch_name
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getProcedureById = async function(id){
    let qry = "SELECT * FROM procedures_tbl WHERE id = ?"
    const resQry = await conn.findFirst(qry,[id]).then((res)=>res);
    if(resQry){
        return resQry
    }
    else{
        return false
    }
}

const getProcedureParents = async function(req){
    let qry = "SELECT * FROM `procedures_tbl` WHERE parent_id = 0"
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            procedure_name  : item.procedure_name,
            procedure_desc  : item.procedure_desc,
            date_created     : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getSecretaries = async function(req){
    let e = req.body

    let qry = "SELECT a.id,a.`branch_id`,a.fname,a.mname,a.lname,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname,b.`branch_name` FROM `secretaries_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.`branch_id` WHERE a.is_active = '1' ORDER BY a.lname,a.fname,a.mname"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_id       : item.branch_id,
            branch_name       : item.branch_name,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            date_created    : item.date_created
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getExpenseCategories = async function(){
    let qry = "SELECT * FROM `expenses_category_tbl` WHERE is_active = '1' ORDER BY id"
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                          : item.id,
            expense_category_name       : item.expense_category_name,
            is_active                   : item.is_active,
            date_created                : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getProceduresRanks = async function(req){
    let e = req.body

    let qry = "SELECT a.id,b.`procedure_name` AS category,a.`procedure_name`,a.`procedure_desc`,a.`price`,a.`commission_type`,a.`commission_rate`,a.assistants_rate,a.assistants_type,a.secretary_rate,a.secretary_type,a.isDentistAllowed,a.isAssistantAllowed,a.isSecretaryAllowed,a.haveQuantity,a.quantityComsThreshold,a.greaterThanQty,a.lessThanQty FROM `procedures_tbl` a INNER JOIN procedures_tbl b ON b.id = a.parent_id WHERE a.`parent_id` <> 0"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                       : item.id,
            category                 : item.category,
            procedure_name           : item.procedure_name,
            commission_type          : 'percentage',
            commission_rate          : 0,

        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getRanks = async function(req){
    let qry = "SELECT id,rank_name,is_active,date_created FROM `ranks_tbl` WHERE is_active = '1'"
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                       : item.id,
            rank_name                : item.rank_name,
            is_active                : item.is_active,
            date_created             : item.date_created,
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getModeOfPayments = async function(req){
    let e = req.body

    let qry = "SELECT * FROM `mode_of_payment_tbl` ORDER BY id"
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                       : item.id,
            mop                      : item.mop,
            date_created             : item.date_created,
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}


module.exports = {
    getPatients,
    getDoctors,
    getProcedures,
    getAssistants,
    getBranchLookup,
    getProcedureById,
    getProcedureParents,
    getSecretaries,
    getExpenseCategories,
    getProceduresRanks,
    getRanks,
    getModeOfPayments
}

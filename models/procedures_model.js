var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

//#region BRANCHES

const get = async function(req){
    let e = req.body
    let row = e.row
    let page = e.page
    let offset = (page-1)*row
    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " AND a.procedure_name LIKE '%"+search+"%'"
    }

    let qry = "SELECT a.*,b.`procedure_name` AS category FROM `procedures_tbl` a INNER JOIN procedures_tbl b ON b.id = a.parent_id WHERE a.parent_id <> 0 "+searchQry+"LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[row,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            parent_id       : item.parent_id,
            category        : item.category,
            procedure_name  : item.procedure_name,
            procedure_desc  : item.procedure_desc,
            commission_type  : item.commission_type,
            commission_rate  : item.commission_rate,
            assistants_rate  : item.assistants_rate,
            assistants_type  : item.assistants_type,
            secretary_rate      : item.secretary_rate,
            secretary_type  : item.secretary_type,
            isDentistAllowed  : item.isDentistAllowed,
            isAssistantAllowed  : item.isAssistantAllowed,
            isSecretaryAllowed  : item.isSecretaryAllowed,
            haveQuantity        : item.haveQuantity,
            quantityComsThreshold : item.quantityComsThreshold,
            greaterThanQty : item.greaterThanQty,
            lessThanQty  : item.lessThanQty,
            procedure_quantity_req   : await getProcedureQtyReqTbl(item.id),
            date_created     : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getProcedureQtyReqTbl = async function(id){
    let qry = "SELECT id,procedure_id,qty,dentist_rate,dentist_type,assistant_rate,assistant_type,secretary_rate,secretary_type FROM `procedures_qty_req_tbl` WHERE procedure_id = ?"
    const resQry = await conn.getQuery(qry,[id]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                          : item.id,
            procedure_id                : item.procedure_id,
            qty                         : item.qty,
            dentist_rate                : item.dentist_rate,
            dentist_type                : item.dentist_type,
            assistant_rate              : item.assistant_rate,
            assistant_type              : item.assistant_type,
            secretary_rate              : item.secretary_rate,
            secretary_type              : item.secretary_type,
            date                        : moment(item.date).format("YYYY-MM-DD")
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getTotal = async function(req){
    let e = req.body
    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " WHERE procedure_name LIKE '%"+search+"%'"
    }

    let qry = "SELECT COUNT(id) as cnt FROM `procedures_tbl`" + searchQry

    let resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const save = async function(req){
    let e = req.body
    let mode = e.mode
    
    if(mode=="add"){
        console.log(e.procedure_quantity_req)
        let ins =  "INSERT INTO `procedures_tbl`(parent_id,procedure_name,procedure_desc,commission_type,commission_rate,assistants_rate,assistants_type,secretary_rate,secretary_type,isDentistAllowed,isAssistantAllowed,isSecretaryAllowed,haveQuantity,quantityComsThreshold,greaterThanQty,lessThanQty,created_by)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        const resIns = await conn.executeQuery(ins,[e.parent_id,e.procedure_name,e.procedure_desc,e.commission_type,e.commission_rate,e.assistants_rate,e.assistants_type,e.secretary_rate,e.secretary_type,e.isDentistAllowed,e.isAssistantAllowed,e.isSecretaryAllowed,e.haveQuantity,e.quantityComsThreshold,e.greaterThanQty,e.lessThanQty,e.created_by]).then((res)=>res);
    
        if(resIns){
            let proc_qty_tbl = await saveProcedureQtyReq(e.procedure_quantity_req,resIns.insertId)
            if(proc_qty_tbl){
                return true
            }
            else{
                return false
            }
        }
        else{
            return false
        }
    }
    else{
        let upd = "UPDATE procedures_tbl SET parent_id=?,procedure_name=?,procedure_desc=?,commission_type=?,commission_rate=?,assistants_rate=?,assistants_type=?,secretary_rate=?,secretary_type=?,isDentistAllowed=?,isAssistantAllowed=?,isSecretaryAllowed=?,haveQuantity=?,quantityComsThreshold=?,greaterThanQty=?,lessThanQty=? WHERE id = ?"
        const resIns = await conn.executeQuery(upd,[e.parent_id,e.procedure_name,e.procedure_desc,e.commission_type,e.commission_rate,e.assistants_rate,e.assistants_type,e.secretary_rate,e.secretary_type,e.isDentistAllowed,e.isAssistantAllowed,e.isSecretaryAllowed,e.haveQuantity,e.quantityComsThreshold,e.greaterThanQty,e.lessThanQty,e.key]).then((res)=>res);
    
        if(resIns){
            let proc_qty_tbl = await saveProcedureQtyReq(e.procedure_quantity_req,e.key)
            if(proc_qty_tbl){
                return true
            }
            else{
                return false
            }
        }
        else{
            return false
        }
    }
}

const categorySave = async function(req){
    let e = req.body
    let mode = e.mode
    
    if(mode=="add"){
        let ins =  "INSERT INTO `procedures_tbl`(parent_id,procedure_name,procedure_desc,created_by)VALUES(?,?,?,?)"
        const resIns = await conn.executeQuery(ins,[0,e.category_name,e.category_desc,e.created_by]).then((res)=>res);
    
        if(resIns){
            return true
        }
        else{
            return false
        }
    }
    else{
        let upd = "UPDATE procedures_tbl SET procedure_name=?,procedure_desc=? WHERE id = ?"
        const resIns = await conn.executeQuery(upd,[e.category_name,e.category_desc,e.key]).then((res)=>res);
    
        if(resIns){
            return true
        }
        else{
            return false
        }
    }
}

const categoryGet = async function(req){
    let e = req.body
    let row = e.rows
    let page = e.page
    let offset = (page-1)*row
    let search = e.search
    let searchQry = ""

    if(search!=""){
        searchQry = " AND procedure_name LIKE '%"+search+"%'"
    }

    let qry = "SELECT * FROM `procedures_tbl` WHERE parent_id = 0 "+searchQry+"LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[row,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            parent_id       : item.parent_id,
            procedure_name  : item.procedure_name,
            procedure_desc  : item.procedure_desc,
            date_created     : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getTotalCategory = async function(req){
    let e = req.body
    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " AND procedure_name LIKE '%"+search+"%'"
    }

    let qry = "SELECT COUNT(id) as cnt FROM `procedures_tbl` WHERE parent_id = 0" + searchQry

    let resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const saveProcedureQtyReq = async function(arr,procedure_id){
    let del = "DELETE FROM procedures_qty_req_tbl WHERE procedure_id = ?"
    const resdel = await conn.executeQuery(del,[procedure_id]).then((res)=>res);

    for await(let item of arr){
        let qry = "INSERT INTO `procedures_qty_req_tbl`(procedure_id,qty,dentist_rate,dentist_type,assistant_rate,assistant_type,secretary_rate,secretary_type)VALUES(?,?,?,?,?,?,?,?)"
        const res = await conn.executeQuery(qry,[procedure_id,item.qty,item.dentist_rate,item.dentist_type,item.assistant_rate,item.assistant_type,item.secretary_rate,item.secretary_type]).then((res)=>res);
    }

    return true
}


module.exports = {
    get,
    save,
    getTotal,
    categorySave,
    categoryGet,
    getTotalCategory
}
var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

//#region BRANCHES

const getCategories = async function(req){
    let e = req.body
    let row = e.row
    let page = e.page
    let offset = (page-1)*row
    let qry = "SELECT * FROM `expenses_category_tbl` WHERE is_active = '1' ORDER BY expense_category_name"

    const resQry = await conn.getQuery(qry,[row,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                              : item.id,
            expense_category_name           : item.expense_category_name,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const get = async function(req){
    let e = req.body
    let rows = e.rows
    let page = e.page
    let offset = (page-1)*rows
    let search = e.search
    let searchQry = ""
    let branch = e.branch || null
    let branchQry = ""
    
    if(search!=""){
        searchQry = " AND a.expense_name LIKE '%"+search+"%'"
    }

    if(branch!==null){
        branchQry = " AND a.branch_id = " + branch + " "
    }

    let qry = "SELECT a.id,a.`branch_id`,b.`branch_name`,a.`amount`,a.`date_created`,a.`expense_name`,c.`expense_category_name`,a.`expense_category_id`,a.is_company_expense,a.covered_date FROM `expenses_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.`branch_id` INNER JOIN `expenses_category_tbl` c ON c.id = a.`expense_category_id` WHERE a.is_active = '1'"+searchQry+branchQry+" ORDER BY a.id DESC LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                      : item.id,
            branch_id               : item.branch_id,
            branch_name             : item.branch_name,
            amount                  : item.amount,
            expense_name            : item.expense_name,
            expense_category_name   : item.expense_category_name,
            expense_category_id     : item.expense_category_id,
            is_company_expense      : item.is_company_expense,
            covered_date            : moment(item.covered_date).format("ll"),
            covered_date_raw        : moment(item.covered_date).format("YYYY-MM-DD"),
            date_created            : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }  
}

const getTotal = async function(req){
    let e = req.body
    let search = e.search
    let searchQry = ""
    let branch = e.branch || null
    let branchQry = ""

    if(search!=""){
        searchQry = " AND expense_name LIKE '%"+search+"%'"
    }

    if(branch!==null){
        branchQry = " AND branch_id = " + branch + " "
    }

    let qry = "SELECT COUNT(id) as cnt FROM `expenses_tbl` WHERE is_active = '1'" + searchQry + branchQry

    let resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const save = async function(req){
    let e = req.body
    let mode                = e.mode
    let key                 = e.key
    let branch              = e.branch
    let label               = e.label
    let amount              = e.amount
    let category            = e.category
    let date                = moment(e.date).format("YYYY-MM-DD")
    let is_company_expense  = e.is_company_expense

    if(mode=="add"){
        let qry = "INSERT INTO `expenses_tbl`(branch_id,expense_category_id,expense_name,amount,is_company_expense,covered_date,created_by)VALUES(?,?,?,?,?,?,?)"
        let resQry = await conn.executeQuery(qry,[branch,category,label,amount,is_company_expense,date,e.created_by]).then((res)=>res);
    
        if(resQry){
            return true
        }
        else{
            return false
        }
    }
    else{
        let qry = "UPDATE expenses_tbl SET branch_id=?,expense_category_id=?,expense_name=?,amount=?,is_company_expense=?,covered_date=? WHERE id = ?"
        let resQry = await conn.executeQuery(qry,[branch,category,label,amount,is_company_expense,date,key]).then((res)=>res);
    
        if(resQry){
            return true
        }
        else{
            return false
        }    
    }
}

const saveCategories = async function(req){
    let e = req.body
    let mode = e.mode
    let key = e.key
    let expense_category_name = e.expense_category_name
    let is_active = e.is_active

    if(mode=="add"){
        let qry = "INSERT INTO `expenses_category_tbl`(expense_category_name,is_active,created_by)VALUES(?,?,?)"
        let resQry = await conn.executeQuery(qry,[expense_category_name,is_active,e.created_by]).then((res)=>res);
        if(resQry){
            return true
        }
        else{
            return false
        }    
    }
    else{
        let qry = "UPDATE expenses_category_tbl SET expense_category_name = ?,is_active=? WHERE id = ?"
        let resQry = await conn.executeQuery(qry,[expense_category_name,is_active,key]).then((res)=>res);
        if(resQry){
            return true
        }
        else{
            return false
        }    
    }
}

const getCategoriesList = async function(req){
    let e = req.body
    let rows = e.rows
    let page = e.page
    let offset = (page-1)*rows
    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " WHERE expense_category_name LIKE '%"+search+"%' "
    }

    let qry = "SELECT id,expense_category_name,is_active,date_created FROM expenses_category_tbl "+searchQry+" LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                      : item.id,
            expense_category_name   : item.expense_category_name,
            is_active               : item.is_active,
            date_created            : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }  
}

const getCategoriesTotal = async function(req){
    let e = req.body
    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " WHERE expense_category_name LIKE '%"+search+"%' "
    }

    let qry = "SELECT COUNT(id) as cnt FROM `expenses_category_tbl`"+searchQry
    let resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const deleteRecord = async function(req) {
    let e = req.body

    let qry = "DELETE FROM `expenses_tbl` WHERE id = ?"
    let resQry = await conn.executeQuery(qry,[e.key]).then((res)=>res);

    if(resQry){
        return true
    }
    else{
        return false
    }
}

module.exports = {
    getCategories,
    saveCategories,
    getCategoriesList,
    getCategoriesTotal,
    get,
    getTotal,
    save,
    deleteRecord
}
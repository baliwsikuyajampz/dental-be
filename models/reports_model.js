var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
const { promises } = require("nodemailer/lib/xoauth2")
var backEndUrl = config.URLs.backEnd

//#region BRANCHES

const getProfitAndLoss = async function(req){
    let e = req.body
    let year = moment().year()

    let qry = "SELECT * FROM `branches_lookup_tbl`"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        
        const arr = await resQry.map(async (item,index) => {
            let result = []
            let months = ['01','02','03','04','05','06','07','08','09','10','11','12']

            for await(let month of months){
                let sales = "SELECT IFNULL(SUM(b.amount),0) as total_sales FROM `transactions_tbl` a INNER JOIN transaction_details_tbl b ON a.id = b.transaction_id WHERE a.branch_id = ? AND (b.proc_status <> 'pending' OR b.proc_status <> 'voided') AND LEFT(b.covered_date,7) = CONCAT('"+year+"','-','"+month+"')"
                const resSales= await conn.findFirst(sales,[item.id]).then((res)=>res);
                
                let total_expense = await getExpenseTotalPerMonth(year,month,item.id)
                let total_cash_received = await getTotalCashReceived(year,month,item.id)
                let gross_sales = resSales.total_sales
                let expenses = await getExpensesListWithTotal(year,month,item.id)
                let mop = await getSalesByMOP(year,month,item.id)

                let tmp = {
                    months      : month,
                    month_name  : moment(month).format('MMM'),
                    year        : year,
                    total_sales : gross_sales,
                    expenses        : expenses,
                    mop             : mop,
                    total_expense   : total_expense,
                    net_worth   : gross_sales - total_expense,
                    total_cash_received : total_cash_received
                }
                 console.log("1`")
                 result.push(tmp)
            }

            return {
                id                      : item.id,
                branch_name             : item.branch_name,
                data                    : result
            }
        })

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getExpensesListWithTotal = async function(year,month,branch_id){
    let qry = "SELECT * FROM `expenses_category_tbl` WHERE is_active = '1' ORDER BY id"
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                          : item.id,
            expense_category_name       : item.expense_category_name,
            is_active                   : item.is_active,
            amount                      : await getExpenseTotal(year,month,item.id,branch_id),
            date_created                : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getSalesByMOP = async function(year,month,branch_id){
    let qry = "SELECT * FROM `mode_of_payment_tbl` ORDER BY id"
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                          : item.id,
            mop                         : item.mop,
            amount                      : await getSalesByMOPTotal(year,month,item.mop,branch_id),
            date_created                : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getSalesByMOPTotal = async function(year,month,mop,branch_id){
    let qry = "SELECT IFNULL(SUM(a.amount),0) AS total FROM `transaction_payments_tbl`a INNER JOIN `transactions_tbl` b ON a.`transaction_id` = b.id WHERE b.`branch_id` = ? AND a.mop = ? AND LEFT(a.`payment_date`,7) = CONCAT('"+year+"','-','"+month+"')"
    const resQry = await conn.findFirst(qry,[branch_id,mop]).then((res)=>res);

    return await resQry.total
}

const getTotalCashReceived = async function(year,month,branch_id){
    let qry = "SELECT IFNULL(SUM(a.amount),0) AS total FROM `transaction_payments_tbl`a INNER JOIN `transactions_tbl` b ON a.`transaction_id` = b.id WHERE b.`branch_id` = ? AND LEFT(a.`payment_date`,7) = CONCAT('"+year+"','-','"+month+"')"
    const resQry = await conn.findFirst(qry,[branch_id]).then((res)=>res);

    return await resQry.total
}

const getExpenseTotal = async function(year,month,id,branch_id){
    let qry = "SELECT IFNULL(SUM(amount),0) as total FROM `expenses_tbl` WHERE expense_category_id = ? AND branch_id = ? AND is_company_expense ='1' AND LEFT(covered_date,7) = CONCAT('"+year+"','-','"+month+"')"
    const resQry = await conn.findFirst(qry,[id,branch_id]).then((res)=>res);

    return await resQry.total
}

const getExpenseTotalPerMonth = async function(year,month,branch_id){
    let qry = "SELECT IFNULL(SUM(amount),0) as total FROM `expenses_tbl` WHERE is_company_expense ='1' AND branch_id=? AND LEFT(covered_date,7) = CONCAT('"+year+"','-','"+month+"')"
    const resQry = await conn.findFirst(qry,[branch_id]).then((res)=>res);

    return resQry.total
}

const getCommissions = async function(req){

    let e = req.body

    let qry = "SELECT a.id,a.`branch_id`,b.`branch_name`,a.fname,a.mname,a.lname,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname FROM `dentists_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.branch_id ORDER BY lname,fname,mname"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_name     : item.branch_name,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            commissions     : await getDoctorCommission(item.id,req),
            date_created    : item.date_created
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
    
}

const getAssistantsCommissions = async function(req){
    let e = req.body

    let qry = "SELECT a.id,a.`branch_id`,b.`branch_name`,a.fname,a.mname,a.lname,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname FROM `assistants_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.branch_id ORDER BY lname,fname,mname"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_name     : item.branch_name,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            commissions     : await getAssistantsYearCommission(item.id,req),
            date_created    : item.date_created
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getDoctorCommission = async function(id,req){
    let arr = []
    let cutOffs = await test(req)

    for await(let cutoff of cutOffs){
        let qry = "SELECT IFNULL(SUM(amount),0) AS amount FROM `employee_commissions_tbl` WHERE employee_id = ? AND status='done' AND covered_date BETWEEN ? AND ?"
        const resQry = await conn.findFirst(qry,[id,cutoff.startDate,cutoff.endDate]).then((res)=>res);
        if(resQry){
            arr.push(resQry.amount)
        }

    }

    return arr
}

const getAssistantsYearCommission = async function(id,req){
    let arr = []
    let cutOffs = await test(req)

    for await(let cutoff of cutOffs){
        let qry = "SELECT IFNULL(SUM(amount),0) AS amount FROM `assistants_commissions_tbl` WHERE assistant_id = ? AND status='done' AND covered_date BETWEEN ? AND ?"
        const resQry = await conn.findFirst(qry,[id,cutoff.startDate,cutoff.endDate]).then((res)=>res);
        if(resQry){
            arr.push(resQry.amount)
        }

    }

    return arr
}

const test = async function(req){
    let selectedYear = moment().format("YYYY") 
    let cutOffs = []

    let months = ['01','2','3','4','5','6','7','8','9','10','11','12']

    for await(let month of months){        
        let first = { //26-10
            month : month,
            startDate : moment(month+ " 26 " + selectedYear).subtract(1,"months").format("YYYY-MM-DD"),
            endDate   : moment(month+ " 10 " + selectedYear).format("YYYY-MM-DD"),
            display : moment(month).subtract(1,"months").format("MMM") + " 26 to " +moment(month).format("MMM") + " 10" 
        }

        cutOffs.push(first)

        let second = { //11-25
            month : month,
            startDate : moment(month+ " 11 " + selectedYear).format("YYYY-MM-DD") ,
            endDate   : moment(month+ " 25 " + selectedYear).format("YYYY-MM-DD"),
            display : moment(month).format("MMM") + " 11 to " +  moment(month).format("MMM") + " 25"
        }

        cutOffs.push(second)
    }

    return cutOffs
}

const getSecretaryCommissions = async function(req){
    let e = req.body

    let qry = "SELECT a.id,a.`branch_id`,b.`branch_name`,a.fname,a.mname,a.lname,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname FROM `secretaries_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.branch_id ORDER BY lname,fname,mname"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_name     : item.branch_name,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            commissions     : await getSecretaryYearCommission(item.id,req),
            date_created    : item.date_created
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getSecretaryYearCommission = async function(id,req){
    let arr = []
    let cutOffs = await test(req)

    for await(let cutoff of cutOffs){
        let qry = "SELECT IFNULL(SUM(amount),0) AS amount FROM `secretaries_commissions_tbl` WHERE assistant_id = ? AND status='done' AND covered_date BETWEEN ? AND ?"
        const resQry = await conn.findFirst(qry,[id,cutoff.startDate,cutoff.endDate]).then((res)=>res);
        if(resQry){
            arr.push(resQry.amount)
        }

    }

    return arr
}

const getDailySalesDetails = async function(branch_id,date){
    let qry = "SELECT a.id,a.`procedure_id`,b.`procedure_name`,CONCAT(c.`lname`,', ',c.fname,' ',c.mname) AS dentist_name,a.`amount`,(a.amount * a.qty) as total_amount,a.`proc_status`,a.`commission_distributed`,a.`assistants_commission_distributed`,a.`secretary_commission_distributed`,a.`company_revenue`,a.qty FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` LEFT JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,10) = LEFT(?,10) LIMIT 10 OFFSET 0"

    const resQry = await conn.getQuery(qry,[branch_id,date]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                                  : item.id,
            procedure_name                      : item.procedure_name,
            dentist_name                        : item.dentist_name,
            amount                              : item.amount,
            qty                                 : item.qty,
            total_amount                        : item.total_amount,
            commission_distributed              : item.commission_distributed,
            assistants_commission_distributed   : item.assistants_commission_distributed,
            secretary_commission_distributed    : item.secretary_commission_distributed,
            company_revenue                     : item.company_revenue,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getDailySalesDetailsCount = async function(branch_id,date){
    let qry = "SELECT COUNT(a.id) AS cnt FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` LEFT JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,10) = LEFT(?,10)"

    const resQry = await conn.findFirst(qry,[branch_id,date]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }

}

const getDailySalesDetailsTotal = async function(branch_id,date){
    let qry = "SELECT IFNULL(SUM(a.amount),0) AS payments, IFNULL((SELECT SUM(f.amount) FROM `expenses_tbl` f WHERE LEFT(f.covered_date,10) = LEFT(?, 10) AND f.branch_id = b.branch_id),0) AS expense FROM `transaction_payments_tbl` a INNER JOIN `transactions_tbl` b ON a.`transaction_id` = b.id WHERE b.`branch_id` = ? AND LEFT(a.`payment_date`, 10) = LEFT(?, 10)"

    const resQry = await conn.findFirst(qry,[date,branch_id,date]).then((res)=>res);

    return resQry
}

const getDailySales = async function(req){
    let e = req.body
    let date = e.date
    let branch = e.branch || null
    let branchQry = ""

    if(branch!=null){
        branchQry = " AND id = "+branch
    }

    date = moment(date).format("YYYY-MM-DD")

    let qry = "SELECT * FROM `branches_lookup_tbl` WHERE is_active = '1'" +branchQry

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_code     : item.branch_code,
            branch_name     : item.branch_name,
            details         : await getDailySalesDetails(item.id,date),
            count           : await getDailySalesDetailsCount(item.id,date),
            totals          : await getDailySalesDetailsTotal(item.id,date),
            payments        : await getDailyPayments(item.id,date)
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getDailyPayments = async function(branch_id,date){
    console.log("wew",branch_id,date)
    let qry = "SELECT a.mop,(SELECT IFNULL(SUM(b.amount),0) FROM `transaction_payments_tbl` b INNER JOIN `transactions_tbl` c ON c.id=b.transaction_id WHERE b.mop = a.`mop` AND c.branch_id = ? AND LEFT(b.payment_date,10) = LEFT(?,10) ) AS amount FROM `mode_of_payment_tbl` a "

    const resQry = await conn.getQuery(qry,[branch_id,date]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            mop         : item.mop,
            amount      : item.amount,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getMonthlySales = async function(req){
    let e = req.body

    let qry = "SELECT * FROM `branches_lookup_tbl` WHERE is_active = '1'"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_code     : item.branch_code,
            branch_name     : item.branch_name,
            count           : await getMonthlySalesDetailsCount(item.id),
            details         : await getMonthlySalesDetails(item.id),
            totals          : await getMonthlySalesDetailsTotal(item.id),
            payments        : await getMonthlyPayments(item.id)

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getMonthlySalesDetails = async function(branch_id){
    let qry = "SELECT a.id,a.`procedure_id`,b.`procedure_name`,CONCAT(c.`lname`,', ',c.fname,' ',c.mname) AS dentist_name,a.`amount`,a.qty,a.`proc_status`,a.`commission_distributed`,a.`assistants_commission_distributed`,a.`secretary_commission_distributed`,a.`company_revenue`,(a.amount * a.qty) as total_amount FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` LEFT JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,7) = LEFT(NOW(),7) LIMIT 10 OFFSET 0"

    const resQry = await conn.getQuery(qry,[branch_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                                  : item.id,
            procedure_name                      : item.procedure_name,
            dentist_name                        : item.dentist_name,
            amount                              : item.amount,
            qty                                 : item.qty,
            total_amount                        : item.total_amount,
            commission_distributed              : item.commission_distributed,
            assistants_commission_distributed   : item.assistants_commission_distributed,
            secretary_commission_distributed    : item.secretary_commission_distributed,
            company_revenue                     : item.company_revenue,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getMonthlySalesDetailsCount = async function(branch_id){
    let qry = "SELECT COUNT(a.id) AS cnt FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` LEFT JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,7) = LEFT(NOW(),7)"

    const resQry = await conn.findFirst(qry,[branch_id]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const getMonthlySalesDetailsTotal = async function(branch_id){
    let qry = "SELECT IFNULL(SUM(a.amount), 0) AS payments, IFNULL( (SELECT SUM(f.amount) FROM `expenses_tbl` f WHERE LEFT(f.covered_date, 7) = LEFT(NOW(), 7) AND f.branch_id = b.branch_id), 0 ) AS expense FROM `transaction_payments_tbl` a INNER JOIN `transactions_tbl` b ON a.`transaction_id` = b.id WHERE b.`branch_id` = ? AND LEFT(a.`payment_date`, 7) = LEFT(NOW(), 7)"

    const resQry = await conn.findFirst(qry,[branch_id]).then((res)=>res);

    return resQry
}

const getMonthlyPayments = async function(branch_id){
    let qry = "SELECT a.mop,(SELECT IFNULL(SUM(b.amount),0) FROM `transaction_payments_tbl` b INNER JOIN `transactions_tbl` c ON c.id=b.transaction_id WHERE b.mop = a.`mop` AND c.branch_id = ? AND LEFT(b.payment_date,7) = LEFT(NOW(),7) ) AS amount FROM `mode_of_payment_tbl` a "

    const resQry = await conn.getQuery(qry,[branch_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            mop         : item.mop,
            amount      : item.amount,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getAnnualSales = async function(req){
    let e = req.body

    let qry = "SELECT * FROM `branches_lookup_tbl` WHERE is_active = '1'"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            branch_code     : item.branch_code,
            branch_name     : item.branch_name,
            count           : await getAnnualSalesDetailsCount(item.id),
            details         : await getAnnualSalesDetails(item.id),
            totals          : await getAnnualSalesDetailsTotal(item.id),
            payments        : await getAnnualPayments(item.id)

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getAnnualSalesDetails = async function(branch_id){
    let qry = "SELECT a.id,a.`procedure_id`,b.`procedure_name`,CONCAT(c.`lname`,', ',c.fname,' ',c.mname) AS dentist_name,a.`amount`,a.`proc_status`,a.`commission_distributed`,a.`assistants_commission_distributed`,a.`secretary_commission_distributed`,a.`company_revenue`,a.qty,(a.amount * a.qty) as total_amount FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` LEFT JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,4) = LEFT(NOW(),4) LIMIT 10 OFFSET 0"

    const resQry = await conn.getQuery(qry,[branch_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                                  : item.id,
            procedure_name                      : item.procedure_name,
            dentist_name                        : item.dentist_name,
            amount                              : item.amount,
            qty                                 : item.qty,
            total_amount                        : item.total_amount,
            commission_distributed              : item.commission_distributed,
            assistants_commission_distributed   : item.assistants_commission_distributed,
            secretary_commission_distributed    : item.secretary_commission_distributed,
            company_revenue                     : item.company_revenue,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getAnnualSalesDetailsCount = async function(branch_id){
    let qry = "SELECT COUNT(a.id) AS cnt FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` LEFT JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,4) = LEFT(NOW(),4)"

    const resQry = await conn.findFirst(qry,[branch_id]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const getAnnualSalesDetailsTotal = async function(branch_id){
    let qry = "SELECT IFNULL(SUM(a.amount), 0) AS payments, IFNULL( (SELECT SUM(f.amount) FROM `expenses_tbl` f WHERE LEFT(f.covered_date, 4) = LEFT(NOW(), 4) AND f.branch_id = b.branch_id), 0 ) AS expense FROM `transaction_payments_tbl` a INNER JOIN `transactions_tbl` b ON a.`transaction_id` = b.id WHERE b.`branch_id` = ? AND LEFT(a.`payment_date`, 4) = LEFT(NOW(), 4)"

    const resQry = await conn.findFirst(qry,[branch_id]).then((res)=>res);

    return resQry
}

const getAnnualPayments = async function(branch_id){
    let qry = "SELECT a.mop,(SELECT IFNULL(SUM(b.amount),0) FROM `transaction_payments_tbl` b INNER JOIN `transactions_tbl` c ON c.id=b.transaction_id WHERE b.mop = a.`mop` AND c.branch_id = ? AND LEFT(b.payment_date,4) = LEFT(NOW(),4) ) AS amount FROM `mode_of_payment_tbl` a "

    const resQry = await conn.getQuery(qry,[branch_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            mop         : item.mop,
            amount      : item.amount,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getCommissionsSearch = async function(req){
    let e = req.body
    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let from = moment(e.from).format("YYYY-MM-DD") 
    let to = moment(e.to).format("YYYY-MM-DD") 
    let dentist = e.doctors
    let dentistQry = ""

    if(dentist.length!=0){
        dentistQry = " AND a.employee_id IN("+dentist.toString()+")"
    }

    let qry = "SELECT a.id, a.transaction_id,a.`employee_id`, d.`procedure_name`, (SELECT (amount * qty) FROM `transaction_details_tbl` c WHERE c.transaction_id = a.transaction_id AND c.procedure_id = a.procedure_id) AS total_amount, a.`amount` AS commission, a.`covered_date`, CONCAT(e.`lname`, ', ', e.`fname`) AS dentist, (SELECT CONCAT(f.`lname`, ', ', f.`fname`,' ',f.`mname`) FROM patients_tbl f INNER JOIN transaction_details_tbl g ON g.patient_id = f.id WHERE g.transaction_id = b.id LIMIT 1 ) AS patient FROM `employee_commissions_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` INNER JOIN `procedures_tbl` d ON d.id = a.`procedure_id` INNER JOIN `dentists_tbl` e ON a.`employee_id` = e.id WHERE a.status = 'done' AND CAST(a.covered_date AS DATE) BETWEEN CAST(? AS DATE) AND CAST(? AS DATE) "+dentistQry+" ORDER BY a.id LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[from,to,rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                                  : item.id,
            procedure_name                      : item.procedure_name,
            total_amount                        : item.total_amount,
            commission                          : item.commission,
            covered_date                        : moment(item.covered_date).format("YYYY-MM-DD"),
            dentist                             : item.dentist,
            patient                             : item.patient,
            trans_id                            : item.transaction_id

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getCommissionsSearchTotal = async function(req){
    let e = req.body
    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let from = moment(e.from).format("YYYY-MM-DD") 
    let to = moment(e.to).format("YYYY-MM-DD") 
    let dentist = e.doctors
    let dentistQry = ""

    if(dentist.length!=0){
        dentistQry = " AND a.employee_id IN("+dentist.toString()+")"
    }

    let qry = "SELECT COUNT(a.id) AS cnt FROM `employee_commissions_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` INNER JOIN `procedures_tbl` d ON d.id = a.`procedure_id` INNER JOIN `dentists_tbl` e ON a.`employee_id` = e.id WHERE a.status = 'done' AND CAST(a.covered_date AS DATE) BETWEEN CAST(? AS DATE) AND CAST(? AS DATE) "+dentistQry

    const resQry = await conn.findFirst(qry,[from,to]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const getAssistantsCommissionsSearch = async function(req){
    let e = req.body
    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let from = moment(e.from).format("YYYY-MM-DD") 
    let to = moment(e.to).format("YYYY-MM-DD") 
    let assistants = e.assistants
    let assistantsQry = ""

    if(assistants.length!=0){
        assistantsQry = " AND a.assistant_id IN("+assistants.toString()+")"
    }

    let qry = "SELECT a.id, a.`assistant_id`, d.`procedure_name`, (SELECT (amount * qty) FROM `transaction_details_tbl` c WHERE c.transaction_id = a.transaction_id AND c.procedure_id = a.procedure_id) AS total_amount, a.`amount` AS commission, a.`covered_date`, CONCAT(e.`lname`, ', ', e.`fname`) AS assistant, (SELECT CONCAT(f.`lname`, ', ', f.`fname`,' ',f.`mname`) FROM patients_tbl f INNER JOIN transaction_details_tbl g ON g.patient_id = f.id WHERE g.transaction_id = b.id LIMIT 1 ) AS patient FROM `assistants_commissions_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` INNER JOIN `procedures_tbl` d ON d.id = a.`procedure_id` INNER JOIN `assistants_tbl` e ON a.`assistant_id` = e.id WHERE a.status = 'done' AND CAST(a.covered_date AS DATE) BETWEEN CAST(? AS DATE) AND CAST(? AS DATE) "+assistantsQry+" ORDER BY a.id LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[from,to,rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            procedure_name                      : item.procedure_name,
            total_amount                        : item.total_amount,
            commission                          : item.commission,
            covered_date                        : moment(item.covered_date).format("YYYY-MM-DD"),
            assistant                           : item.assistant,
            patient                             : item.patient,
            
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getAssistantsCommissionsSearchTotal = async function(req){
    let e = req.body
    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let from = moment(e.from).format("YYYY-MM-DD") 
    let to = moment(e.to).format("YYYY-MM-DD") 
    let assistants = e.assistants
    let assistantsQry = ""

    if(assistants.length!=0){
        assistantsQry = " AND a.assistant_id IN("+assistants.toString()+")"
    }

    let qry = "SELECT COUNT(a.id) AS cnt FROM `assistants_commissions_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` INNER JOIN `procedures_tbl` d ON d.id = a.`procedure_id` INNER JOIN `assistants_tbl` e ON a.`assistant_id` = e.id WHERE a.status = 'done' AND CAST(a.covered_date AS DATE) BETWEEN CAST(? AS DATE) AND CAST(? AS DATE) "+assistantsQry

    const resQry = await conn.findFirst(qry,[from,to]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const getSecretaryCommissionsSearch = async function(req){
    let e = req.body
    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let from = moment(e.from).format("YYYY-MM-DD") 
    let to = moment(e.to).format("YYYY-MM-DD") 
    let secretaries = e.secretaries
    let secretariesQry = ""

    if(secretaries.length!=0){
        secretariesQry = " AND a.assistant_id IN("+secretaries.toString()+")"
    }

    let qry = "SELECT a.id, a.`assistant_id`, d.`procedure_name`, (SELECT (amount * qty) FROM `transaction_details_tbl` c WHERE c.transaction_id = a.transaction_id AND c.procedure_id = a.procedure_id) AS total_amount, a.`amount` AS commission, a.`covered_date`, CONCAT(e.`lname`, ', ', e.`fname`) AS secretary, (SELECT CONCAT(f.`lname`, ', ', f.`fname`,' ',f.`mname`) FROM patients_tbl f INNER JOIN transaction_details_tbl g ON g.patient_id = f.id WHERE g.transaction_id = b.id LIMIT 1 ) AS patient FROM `secretaries_commissions_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` INNER JOIN `procedures_tbl` d ON d.id = a.`procedure_id` INNER JOIN `secretaries_tbl` e ON a.`assistant_id` = e.id WHERE a.status = 'done' AND CAST(a.covered_date AS DATE) BETWEEN CAST(? AS DATE) AND CAST(? AS DATE) "+secretariesQry+" ORDER BY a.id LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[from,to,rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            procedure_name                      : item.procedure_name,
            total_amount                        : item.total_amount,
            commission                          : item.commission,
            covered_date                        : moment(item.covered_date).format("YYYY-MM-DD"),
            secretary                           : item.secretary,
            patient                             : item.patient,

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getSecretaryCommissionsSearchTotal = async function(req){
    let e = req.body
    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let from = moment(e.from).format("YYYY-MM-DD") 
    let to = moment(e.to).format("YYYY-MM-DD") 
    let secretaries = e.secretaries
    let secretariesQry = ""

    if(secretaries.length!=0){
        secretariesQry = " AND a.assistant_id IN("+secretaries.toString()+")"
    }

    let qry = "SELECT COUNT(a.id) AS cnt FROM `secretaries_commissions_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` INNER JOIN `procedures_tbl` d ON d.id = a.`procedure_id` INNER JOIN `secretaries_tbl` e ON a.`assistant_id` = e.id WHERE a.status = 'done' AND CAST(a.covered_date AS DATE) BETWEEN CAST(? AS DATE) AND CAST(? AS DATE) "+secretariesQry

    const resQry = await conn.findFirst(qry,[from,to]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const getDailySalesDetailsByPageChange = async function(req){
    let e = req.body
    let page = e.page
    let rows = 10
    let offset = (page-1)*rows
    let branch_id = e.branch
    let date = e.date

    let qry = "SELECT a.id,a.`procedure_id`,b.`procedure_name`,CONCAT(c.`lname`,', ',c.fname,' ',c.mname) AS dentist_name,a.`amount`,(a.amount * a.qty) as total_amount,a.`proc_status`,a.`commission_distributed`,a.`assistants_commission_distributed`,a.`secretary_commission_distributed`,a.`company_revenue`,a.qty FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` INNER JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,10) = LEFT(?,10) LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[branch_id,date,rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                                  : item.id,
            procedure_name                      : item.procedure_name,
            dentist_name                        : item.dentist_name,
            amount                              : item.amount,
            qty                                 : item.qty,
            total_amount                        : item.total_amount,
            commission_distributed              : item.commission_distributed,
            assistants_commission_distributed   : item.assistants_commission_distributed,
            secretary_commission_distributed    : item.secretary_commission_distributed,
            company_revenue                     : item.company_revenue,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getMonthlySalesDetailsByPageChange = async function(req){
    let e = req.body
    let page = e.page
    let rows = 10
    let offset = (page-1)*rows
    let branch_id = e.branch

    let qry = "SELECT a.id,a.`procedure_id`,b.`procedure_name`,CONCAT(c.`lname`,', ',c.fname,' ',c.mname) AS dentist_name,a.`amount`,(a.amount * a.qty) as total_amount,a.`proc_status`,a.`commission_distributed`,a.`assistants_commission_distributed`,a.`secretary_commission_distributed`,a.`company_revenue`,a.qty FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` INNER JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,7) = LEFT(NOW(),7) LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[branch_id,rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                                  : item.id,
            procedure_name                      : item.procedure_name,
            dentist_name                        : item.dentist_name,
            amount                              : item.amount,
            qty                                 : item.qty,
            total_amount                        : item.total_amount,
            commission_distributed              : item.commission_distributed,
            assistants_commission_distributed   : item.assistants_commission_distributed,
            secretary_commission_distributed    : item.secretary_commission_distributed,
            company_revenue                     : item.company_revenue,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getAnnualSalesDetailsByPageChange = async function(req){
    let e = req.body
    let page = e.page
    let rows = 10
    let offset = (page-1)*rows
    let branch_id = e.branch

    let qry = "SELECT a.id,a.`procedure_id`,b.`procedure_name`,CONCAT(c.`lname`,', ',c.fname,' ',c.mname) AS dentist_name,a.`amount`,(a.amount * a.qty) as total_amount,a.`proc_status`,a.`commission_distributed`,a.`assistants_commission_distributed`,a.`secretary_commission_distributed`,a.`company_revenue`,a.qty FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id=a.`procedure_id` INNER JOIN dentists_tbl c ON c.id = a.`dentist_id` INNER JOIN transactions_tbl d ON d.id = a.transaction_id WHERE d.branch_id=? AND a.`proc_status` IN('done','paid-pending') AND LEFT(a.covered_date,4) = LEFT(NOW(),4) LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[branch_id,rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                                  : item.id,
            procedure_name                      : item.procedure_name,
            dentist_name                        : item.dentist_name,
            amount                              : item.amount,
            qty                                 : item.qty,
            total_amount                        : item.total_amount,
            commission_distributed              : item.commission_distributed,
            assistants_commission_distributed   : item.assistants_commission_distributed,
            secretary_commission_distributed    : item.secretary_commission_distributed,
            company_revenue                     : item.company_revenue,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

module.exports = {
    getProfitAndLoss,
    getCommissions,
    getAssistantsCommissions,
    getSecretaryCommissions,
    test,
    getDailySales,
    getMonthlySales,
    getAnnualSales,
    getCommissionsSearch,
    getCommissionsSearchTotal,
    getAssistantsCommissionsSearch,
    getAssistantsCommissionsSearchTotal,
    getSecretaryCommissionsSearch,
    getSecretaryCommissionsSearchTotal,
    getDailySalesDetailsByPageChange,
    getMonthlySalesDetailsByPageChange,
    getAnnualSalesDetailsByPageChange
}


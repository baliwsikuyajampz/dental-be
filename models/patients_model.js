var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

//#region BRANCHES

const get = async function(req){
    let e = req.body
    let row = e.row
    let page = e.page
    let offset = (page-1)*row
    let search = e.search
    let searchQry = ""

    if(search!=""){
        searchQry = " WHERE CONCAT(a.lname,', ',a.fname,' ',a.mname) LIKE '%"+search+"%'"
    }

    let qry = "SELECT a.id,a.branch_id,c.branch_name,a.fname,a.mname,a.lname,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname,b.`birthday`,b.`contact_no`,b.`address`,b.gender,b.nationality,b.religion,b.occupation,b.office_no,b.nickname,a.is_active,b.guardian_occupation,b.guardian_name FROM `patients_tbl` a INNER JOIN `patient_info_tbl` b ON b.patient_id = a.id LEFT JOIN branches_lookup_tbl c ON c.id = a.branch_id "+searchQry+" ORDER BY a.id DESC LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[row,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            birthday        : moment(item.birthday).format("MMM DD, YYYY"),
            birthday_raw    : moment(item.birthday).format("MM-DD-YYYY"),
            contact_no      : item.contact_no,
            address         : item.address,
            gender         : item.gender,
            nationality         : item.nationality,
            religion         : item.religion,
            occupation         : item.occupation,
            office_no         : item.office_no,
            nickname         : item.nickname,
            is_active       : item.is_active,
            guardian_name : item.guardian_name,
            guardian_occupation : item.guardian_occupation,
            branch_id           : item.branch_id,
            branch_name         : item.branch_name,
            date_created    : moment(item.date_created).format("MMM DD, YYYY")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getTotal = async function(req){
    let e = req.body
    let search = e.search
    let searchQry = ""

    if(search!=""){
        searchQry = " WHERE CONCAT(lname,', ',fname,' ',mname) LIKE '%"+search+"%'"
    }

    let qry = "SELECT COUNT(id) as cnt FROM `patients_tbl`" + searchQry
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    return 0
}

const save = async function(req){
    let e = req.body
    let mode = e.mode

    let bday = e.bday
    bday = moment(bday).format("YYYY-MM-DD")

    if(mode=="add"){

        let checkQry = "SELECT COUNT(a.id) AS cnt FROM `patients_tbl` a INNER JOIN `patient_info_tbl` b ON a.id = b.`patient_id` WHERE a.fname = TRIM(?) AND a.mname = TRIM(?) AND a.lname = TRIM(?) AND LEFT(b.`birthday`,10) = ?"
        const resCheckQry = await conn.findFirst(checkQry,[e.fname,e.mname,e.lname,bday]).then((res)=>res);
        console.log(e.fname,e.lname,bday)
        if(resCheckQry){
            if(resCheckQry.cnt!=0){
                return "exists"
            }
            else{
                let ins =  "INSERT INTO patients_tbl(fname,mname,lname,created_by,branch_id)VALUES(?,?,?,?,?)"
                const resIns = await conn.executeQuery(ins,[e.fname,e.mname,e.lname,e.created_by,e.branch]).then((res)=>res);
            
                if(resIns){
                    let bday = e.bday
                    bday = moment(bday).format("YYYY-MM-DD")
                    let insPatientInfo = "INSERT INTO patient_info_tbl(patient_id,birthday,gender,address,nationality,religion,occupation,office_no,nickname,contact_no,guardian_name,guardian_occupation)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)"
                    const resinsPatientInfo = await conn.executeQuery(insPatientInfo,[resIns.insertId,bday,e.gender,e.address,e.nationality,e.religion,e.occupation,e.office_no,e.nickname,e.contact_no,e.guardian_name,e.guardian_occupation]).then((res)=>res);
                    if(resinsPatientInfo){
                        return true
                    }
                    else{
                        return false
                    }
                }
                else{
                    return false
                }
            }
        }
    }
    else{
        let checkQry = "SELECT COUNT(a.id) AS cnt FROM `patients_tbl` a INNER JOIN `patient_info_tbl` b ON a.id = b.`patient_id` WHERE a.fname = TRIM(?) AND a.mname = TRIM(?) AND a.lname = TRIM(?) AND LEFT(b.`birthday`,10) = ? AND a.id <> ?"
        const resCheckQry = await conn.findFirst(checkQry,[e.fname,e.mname,e.lname,bday,e.key]).then((res)=>res);
        console.log(e.fname,e.lname,bday)
        if(resCheckQry){
            if(resCheckQry.cnt!=0){
                return "exists"
            }
            else{
                let ins =  "UPDATE patients_tbl SET fname=?,mname=?,lname=?,is_active=?,branch_id=? WHERE id = ? "
                const resIns = await conn.executeQuery(ins,[e.fname,e.mname,e.lname,e.is_active,e.branch,e.key]).then((res)=>res);
        
                if(resIns){
                    let bday = e.bday
                    bday = moment(bday).format("YYYY-MM-DD")
                    let ins2 = "UPDATE patient_info_tbl SET birthday=?,gender=?,address=?,nationality=?,religion=?,occupation=?,office_no=?,nickname=?,contact_no=? WHERE patient_id = ? "
                    const resinsPatientInfo = await conn.executeQuery(ins2,[bday,e.gender,e.address,e.nationality,e.religion,e.occupation,e.office_no,e.nickname,e.contact_no,e.key]).then((res)=>res);
                    if(resinsPatientInfo){
                        return true
                    }
                    else{
                        return false
                    }
                }
                else{
                    return false
                }
            }
        }
    }
}

const getPatientById = async function(req){
    let e = req.body

    let qry = "SELECT id,fname,mname,lname,date_created,CONCAT(lname,', ',fname,' ',mname) as fullname,profile_img FROM `patients_tbl` WHERE id = ?"

    const resQry = await conn.findFirst(qry,[e.id]).then((res)=>res);

    if(resQry){
        obj = {
            id                : resQry.id,
            fname             : resQry.fname,
            mname             : resQry.mname,
            lname             : resQry.lname,
            fullname          : resQry.fullname,
            file_name         : resQry.profile_img,
            profile_img       : resQry.profile_img == null ? '' : backEndUrl +"/"+resQry.id+"/"+resQry.profile_img,
            info              : await getPatientInfoById(req),
            date_created      : resQry.date_created,

        }
        return obj
    }
    else{
        return false
    }

}

const getPatientInfoById = async function(req){
    let e = req.body

    let qry = "SELECT patient_id,email,contact_no,address,patient_about FROM patient_info_tbl WHERE patient_id = ?"

    const resQry = await conn.findFirst(qry,[e.id]).then((res)=>res);

    if(resQry){
        return resQry
    }
    else{
        return ""
    }
}

const searchPatientByName = async function(req){
    let e = req.body

    let fname = e.fname
    let mname = e.mname
    let lname = e.lname 

    let page = e.page 
    let rows = e.rows
    let offset = (page-1)*rows

    let fullname = lname +' '+fname+' '+mname

    let qry = "SELECT a.id,CONCAT(a.`lname`,', ',a.fname,' ',a.`mname`) AS fullname,b.`birthday`,b.`contact_no`,b.`email`,b.`address` FROM `patients_tbl` a INNER JOIN `patient_info_tbl` b ON a.id =b.`patient_id` WHERE a.is_active = '1' AND CONCAT(a.`lname`,' ',a.fname,' ',a.`mname`) LIKE '%"+fullname.trim()+"%' ORDER BY a.id DESC LIMIT ? OFFSET ?"
    
    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            fullname        : item.fullname,
            birthday        : moment(item.birthday).format("MMMM DD, YYYY"),
            address         : item.address,
            contact_no         : item.contact_no,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }

}

const searchPatientByNameCount = async function(req){
    let e = req.body

    let fname = e.fname
    let mname = e.mname
    let lname = e.lname 

    let fullname = lname +' '+fname+' '+mname

    let qry = "SELECT COUNT(a.id) AS cnt FROM `patients_tbl` a INNER JOIN `patient_info_tbl` b ON a.id =b.`patient_id` WHERE a.is_active = '1' AND CONCAT(a.`lname`,' ',a.fname,' ',a.`mname`) LIKE '%"+fullname.trim()+"%'"
    
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);
    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const saveUploadAttachment = async function(req){
    let e = req.body

    let qry = "INSERT INTO `patients_attachments_tbl`(patient_id,file_name)VALUES(?,?)"
    const resQry = await conn.executeQuery(qry,[e.patient_id,e.file_name]).then((res)=>res);

    if(resQry){
        return true
    }
    else{
        return false
    }
}

const getUploadAttachment = async function(req){
    let e = req.body

    let qry = "SELECT id,patient_id,file_name,date_created FROM `patients_attachments_tbl` WHERE patient_id = ? "
    
    const resQry = await conn.getQuery(qry,[e.key]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            patient_id      : item.patient_id,
            file_name       : backEndUrl +"/"+item.patient_id+"/"+item.file_name,
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const saveUploadProfile = async function(req){
    let e = req.body

    let qry = "UPDATE `patients_tbl` SET profile_img = ? WHERE id = ?"
    const resQry = await conn.executeQuery(qry,[e.file_name,e.patient_id]).then((res)=>res);

    if(resQry){
        return true
    }
    else{
        return false
    }
}

const updatePatientAbout = async function(req){
    let e = req.body

    let qry = "UPDATE `patient_info_tbl` SET patient_about = ? WHERE patient_id = ?"
    const resQry = await conn.executeQuery(qry,[e.patient_about,e.patient_id]).then((res)=>res);
    if(resQry){
        return true
    }
    else{
        return false
    }
}

const getPatientTransactionsById = async function(req){
    let e = req.body

    let qry = "SELECT IFNULL( CONCAT(c.`lname`, ', ', c.fname), '' ) AS dentist, b.`procedure_name`, a.`amount`, a.qty, (a.`amount` * a.`qty`) AS total_price, a.covered_date, d.`mop` FROM `transaction_details_tbl` a INNER JOIN `procedures_tbl` b ON b.id = a.`procedure_id` LEFT JOIN `dentists_tbl` c ON c.id = a.`dentist_id` LEFT JOIN `transaction_payments_tbl` d ON d.transaction_id = a.`transaction_id` WHERE a.patient_id = ? ORDER BY a.id DESC LIMIT 10"

    const resQry = await conn.getQuery(qry,[e.patient_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            dentist             : item.dentist,
            procedure_name      : item.procedure_name,
            amount              : item.amount,
            mop                 : item.mop,
            qty                 : item.qty,
            total_price         : item.total_price,
            covered_date        : moment(item.covered_date).format("MMM DD, YYYY")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const deletePatient = async function(req){
    let e = req.body

    let checkIfUsed = "SELECT COUNT(id) AS cnt FROM `transaction_details_tbl` WHERE patient_id = ?"
    const rescheckIfUsed = await conn.findFirst(checkIfUsed,[e.key]).then((res)=>res);

    if(rescheckIfUsed){
        if(rescheckIfUsed.cnt != 0){
            return "exists"
        }
        else{
            let del1 = "DELETE FROM patient_info_tbl WHERE patient_id = ?"
            const resdel1 = await conn.executeQuery(del1,[e.key]).then((res)=>res);

            if(resdel1){
                let del = "DELETE FROM patients_tbl WHERE id = ?"
                const resdel = await conn.executeQuery(del,[e.key]).then((res)=>res);
                if(resdel){
                    return true
                }
            }

        }
    }


}

module.exports = {
    get,
    save,
    getPatientById,
    getPatientInfoById,
    searchPatientByName,
    searchPatientByNameCount,
    getTotal,
    saveUploadAttachment,
    getUploadAttachment,
    saveUploadProfile,
    updatePatientAbout,
    getPatientTransactionsById,
    deletePatient
}
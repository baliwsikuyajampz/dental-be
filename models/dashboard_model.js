var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

const getDailySales = async function(req){
    let e = req.body
    let date = moment().format("YYYY-MM-DD")

    let branch = e.branch || null
    let branchQry = ""

    if(branch!==null){
        branchQry = " AND b.branch_id = " + branch + " "
    }

    let qry = "SELECT IFNULL(SUM(a.amount),0) AS daily_sales FROM `transaction_payments_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` WHERE LEFT(a.payment_date, 10) = ? " + branchQry
    const resQry = await conn.findFirst(qry,[date]).then((res)=>res);

    if(resQry){
        let daily_sales = resQry.daily_sales
        let obj = {
            title: 'Total Sales Today',
            number: daily_sales,
            color: 'blue',
        }
        return obj
    }
    else{
        return 0
    }
}

const getDailyExpenses = async function(req){
    let e = req.body
    let date = moment().format("YYYY-MM-DD")

    let branch = e.branch || null
    let branchQry = ""

    if(branch!==null){
        branchQry = " AND branch_id = " + branch + " "
    }

    let qry = "SELECT IFNULL(SUM(amount),0) as daily_expenses FROM `expenses_tbl` WHERE LEFT(covered_date,10) = ?"  + branchQry
    const resQry = await conn.findFirst(qry,[date]).then((res)=>res);

    if(resQry){
        let daily_expenses = resQry.daily_expenses
        let obj = {
            title: 'Total Expenses Today',
            number: daily_expenses,
            color: 'blue',
        }
        return obj
    }
    else{
        return 0
    }

    return date
}

const getMonthlySales = async function(req){
    let monthYear = moment().format("YYYY-MM")
    let e = req.body

    let qry = "SELECT * FROM `branches_lookup_tbl` WHERE is_active = '1'"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        let series = []
        let labels = []

       for await(let item of resQry){
            let data = await getMonthlySalesPerBranch(monthYear,item.id)
            labels.push(item.branch_name)
            series.push(data)
        }

        let tmp = {
            series : series,
            labels : labels
        }

        return tmp
    }
    else{
        return []
    }
}

const getMonthlySalesPerBranch = async function(monthYear,branch_id){
    let qry = "SELECT IFNULL(SUM(b.amount),0) as total FROM `transactions_tbl` a INNER JOIN `transaction_details_tbl` b ON a.id = b.`transaction_id`  WHERE a.`branch_id` = ? AND LEFT(b.`covered_date`,7) = '" + monthYear +"'"
    const resQry = await conn.findFirst(qry,[branch_id]).then((res)=>res);

    return resQry.total

}

const getPendingClients = async function(req){
    let e = req.body
    let branch = e.branch || null
    let branchQry = ""

    if(branch!==null){
        branchQry = " WHERE b.branch_id = " + branch + " "
    }

    let qry = "SELECT IFNULL(SUM(a.`amount`),0) AS amount FROM `transaction_payments_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` "+branchQry
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        let cnt = resQry.amount
        let obj = {
            title: 'Expected Cash on Hand today',
            number: cnt,
            color: 'green',
        }
        return obj
    }

}

const getPendingPatientsList = async function(req){
    let e = req.body
    
    let qry = "SELECT a.id,f.`branch_name`,a.`amount`,CONCAT(b.`lname`,', ',b.fname ,' ',b.`mname`) AS patient_name,c.`procedure_name`,IFNULL(CONCAT(d.`lname`,', ',d.`fname`,' ',d.`mname`),'') AS dentist_name,a.`covered_date`,a.`proc_status` FROM `transaction_details_tbl` a INNER JOIN `patients_tbl` b ON a.`patient_id` = b.id INNER JOIN `procedures_tbl` c ON c.id = a.`procedure_id` LEFT JOIN `dentists_tbl` d ON d.id = a.`dentist_id` INNER JOIN `transactions_tbl` e ON e.id = a.`transaction_id` INNER JOIN `branches_lookup_tbl` f ON f.id = e.`branch_id` WHERE a.proc_status <> 'done' ORDER by a.id DESC LIMIT 5"

    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                      : item.id,
            branch_name             : item.branch_name,
            amount                  : item.amount,
            patient_name            : item.patient_name,
            procedure_name          : item.procedure_name,
            dentist_name            : item.dentist_name,
            proc_status             : item.proc_status,
            date                    : moment(item.covered_date).format("MMM DD, YYYY @ hh:mm a")
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getTotalUnpaidAmount = async function(req){
    let e = req.body
    let branch = e.branch || null
    let branchQry = ""

    if(branch!==null){
        branchQry = " WHERE b.branch_id = " + branch + " "
    }

    let qry = "SELECT IFNULL(SUM(b.amount),0) AS amount FROM `transactions_tbl` b "+branchQry+" UNION SELECT IFNULL(SUM(a.amount),0) AS amount FROM `transaction_payments_tbl` a INNER JOIN `transactions_tbl` b ON a.transaction_id = b.id "+branchQry 
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);
    console.log(resQry.length)
    let amt = 0
    if(resQry.length>1){
        amt = resQry[0].amount - resQry[1].amount
    }

    if(resQry){
        
        let obj = {
            title: 'Total Unpaid Amount',
            number: amt,
            color: 'blue',
        }
        return obj
    }

}

const getGrandTotal = async function(req){
    let e = req.body
    let date = moment().format("YYYY-MM-DD")

    let branch = e.branch || null
    let branchQry = ""

    if(branch!==null){
        branchQry = " AND b.branch_id = " + branch + " "
    }

    let dailySales = "SELECT IFNULL(SUM(a.amount),0) AS daily_sales FROM `transaction_payments_tbl` a INNER JOIN `transactions_tbl` b ON b.id = a.`transaction_id` WHERE LEFT(a.payment_date, 10) = ? " + branchQry
    const resdailySales = await conn.findFirst(dailySales,[date]).then((res)=>res);

    let dailyExpense = "SELECT IFNULL(SUM(b.amount),0) as daily_expenses FROM `expenses_tbl` b WHERE LEFT(b.covered_date,10) = ?"  + branchQry
    const resdailyExpense = await conn.findFirst(dailyExpense,[date]).then((res)=>res);

    if(resdailySales && resdailyExpense){
        let daily_sales = resdailySales.daily_sales - resdailyExpense.daily_expenses
        let obj = {
            title: 'Grand Sales Today',
            number: daily_sales,
            color: 'blue',
        }
        return obj
    }
    else{
        let obj = {
            title: 'Grand Sales Today',
            number: 0,
            color: 'blue',
        }
        return obj
    }
}


module.exports = {
    getDailySales,
    getDailyExpenses,
    getMonthlySales,
    getPendingClients,
    getPendingPatientsList,
    getTotalUnpaidAmount,
    getGrandTotal
    
}
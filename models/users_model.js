var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")

const getUsers = async function(req){
    let page = req.body.page
    let limit = parseInt(req.body.rows)
    let offset = (page-1)*limit

    let qry = "SELECT a.*,b.`role_name`,c.branch_name FROM users_tbl a INNER JOIN `roles_lookup_tbl` b ON b.id = a.`role_id` INNER JOIN branches_lookup_tbl c ON a.branch_id = c.id WHERE a.id <> 1 AND a.is_active ='1' LIMIT ? OFFSET ?"
    const resQry = await conn.getQuery(qry,[limit,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id : item.id,
            fullName : item.lname + ", " + item.fname + " " + item.mname,
            fname : item.fname,
            mname : item.mname,
            lname : item.lname,
            username : item.username,
            role_id : item.role_id,
            role_name : item.role_name,
            branch_id : item.branch_id,
            branch_name : item.branch_name,
            email   : item.email,
            date_created        : moment(item.date_created).format("LLL") //YYYY-MM-DD or LLL,

        }))

        return Promise.all(arr)
    }
    else {
        return []
    }
}

const getUsersCount = async function(req){
    let qry = "SELECT COUNT(a.id) as cnt FROM users_tbl a INNER JOIN `roles_lookup_tbl` b ON b.id = a.`role_id` WHERE a.id <> 1 AND a.is_active ='1'"
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else {
        return false
    }
}

const auth = async function(req){
    let username = req.body.username
    let password = req.body.password
  
    let getQry = "SELECT *,a.id as user_id FROM users_tbl a INNER JOIN roles_lookup_tbl b ON a.role_id = b.id INNER JOIN branches_lookup_tbl c ON c.id = a.branch_id WHERE username = ? AND a.is_active = '1'"
    const resGetQry = await conn.getQuery(getQry,[username]).then((res)=>res);
    if(resGetQry.length == 0){
        return false
    }
    else{
        let checkHash = await base.checkHash(password,resGetQry[0].passhash)
        if(checkHash){
            let e = resGetQry[0]
            let tmp = {


                id  : e.user_id,
                fullName : e.lname + ", " + e.fname + " " + e.mname,
                fname : e.fname,
                mname : e.mname,
                lname : e.lname,
                username : e.username,
                role  : e.role_id,
                role_name : e.role_name,
                branch : e.branch_id,
                email : e.email == null  ? "" : e.email,
                branch_name : e.branch_name,
            }

            return tmp
        }
        else{
            return false
        }
    }
}

const changePass = async function(req){
    let userid = req.body.userid
    let newPassword = req.body.newpass
    let oldPassword = req.body.oldpass

  
    let getQry = "SELECT * FROM users_tbl WHERE id = ?"
    const resGetQry = await conn.getQuery(getQry,[userid]).then((res)=>res);
    if(resGetQry.length == 0){
        return false
    }
    else{
        let checkHash = await base.checkHash(oldPassword,resGetQry[0].passhash)
        if(checkHash){
            let newPasswordHash = await base.encryptStr(newPassword)

            let updatePass = "UPDATE users_tbl set passhash = ? WHERE id = ?"
            const resUpdatePass = await conn.executeQuery(updatePass,[newPasswordHash, userid]).then((res)=>res);
            
            if(resUpdatePass){
                return true
            }else{
                return false
            }
        }
        else{
            return "pass_mismatch"
        }
    }
}

const saveUser = async function(req){
    let fname = req.body.fname
    let mname = req.body.mname
    let lname = req.body.lname
    let mode  = req.body.mode
    let role  = req.body.role
    let branch  = req.body.branch
    let key = req.body.key
    let username = req.body.username
    let created_by = req.body.created_by
    let email = req.body.email
    
    if(mode=="add"){
        let getQry = "SELECT COUNT(id) AS cnt FROM users_tbl WHERE username = ?"
        const resgetQry = await conn.findFirst(getQry,[username]).then((res)=>res);
        if(resgetQry.cnt != 0){
            return "userExist"
        }
        else{
            let insQry = "INSERT INTO users_tbl(role_id,branch_id,username,fname,mname,lname,passhash,email)VALUES(?,?,?,?,?,?,?,?)"
            const resInsQry = await conn.executeQuery(insQry,[role,branch,username,fname,mname,lname,await base.encryptStr(username),email]).then((res)=>res);
            if(resInsQry){
                return true
            }
            else{
                return false
            }
        }
    }
    else{
        let getQry = "SELECT COUNT(id) AS cnt FROM users_tbl WHERE username = ? AND id <> ?"
        const resgetQry = await conn.findFirst(getQry,[username,key]).then((res)=>res);
        if(resgetQry.cnt != 0){
            return "userExist"
        }
        else{
            let insQry = "UPDATE users_tbl SET role_id = ?,branch_id=?, username = ?,fname = ?,mname=?,lname = ?,email=? WHERE id = ?"
            const resInsQry = await conn.executeQuery(insQry,[role,branch,username,fname,mname,lname,email,key]).then((res)=>res);
            if(resInsQry){
                return true
            }
            else{
                return false
            } 
        }
    }

}

const getRoles = async function(req){
    let qry = "SELECT * FROM roles_lookup_tbl WHERE is_active = '1' AND id <> 1"
    const resGetQry = await conn.getQuery(qry,[]).then((res)=>res);
    if(resGetQry){
        return resGetQry
    }
    else{
        return false
    }
}

const deleteUser = async function(req){
    let e = req.body

    let delQry = "UPDATE users_tbl SET is_active = '0' WHERE id = ?"
    const resdelQry = await conn.executeQuery(delQry,[e.id]).then((res)=>res);
    if(resdelQry){
        return true
    }
    else{
        return false
    } 
}

const getUserByIdLogs = async function(req){
    let e = req.body
    let rows = e.rows
    let page = e.page
    let limit = parseInt(rows)
    let offset = (page-1)*limit

    let qry = "SELECT a.fname,a.mname,a.lname,a.`date_created`,a.`role_id`,a.`branch_id`,a.`username`,b.`role_name`,c.`branch_name` FROM users_tbl a INNER JOIN `roles_lookup_tbl` b ON b.id = a.`role_id` INNER JOIN branches_lookup_tbl c ON c.id = a.`branch_id` WHERE a.id = ?"
    const resqry = await conn.findFirst(qry,[e.id]).then((res)=>res);
    if(resqry){
        let qry1 = "SELECT * FROM `audit_trail_tbl` WHERE user_id = ? ORDER BY id DESC LIMIT ? OFFSET ?"
        const resqry1 = await conn.getQuery(qry1,[e.id,rows,offset]).then((res)=>res);
        arr = []

        for await (let item of resqry1 ){
            let tmp = {
                id              : item.id,
                title           : item.title,
                activity        : item.activity,
                lname           : item.lname,
                date_created    : moment(item.date_created).format("LLL") //YYYY-MM-DD or LLL,
            }

            arr.push(tmp)
        }

        let tmp = {
            fname : resqry.fname,
            mname : resqry.mname,
            lname : resqry.lname,
            display_name : resqry.fname + " " + resqry.lname,
            username : resqry.username,
            branch_name : resqry.branch_name,
            role_name : resqry.role_name,
            audit : arr
        }

        return tmp
    }
    return false
}

const getUserByIdLogsCount = async function(req){
    let e = req.body

    let qry1 = "SELECT COUNT(id) as cnt FROM `audit_trail_tbl` WHERE user_id = ?"
    const resqry1 = await conn.getQuery(qry1,[e.id]).then((res)=>res);
    if(resqry1){
        return resqry1.cnt
    }
    else{
        return false
    }
}

module.exports = {
    getUsers,
    auth,
    saveUser,
    changePass,
    getRoles,
    getUsersCount,
    deleteUser,
    getUserByIdLogs,
    getUserByIdLogsCount
}
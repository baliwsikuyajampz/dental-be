var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd
var lookups = require("../models/lookups_model")


const save = async function(req){
    let e = req.body
    let procedures = e.procedures
    let paymentMethod = e.paymentMethod

    let trans_status = e.mop == "PARTIAL PAYMENT" ? "PENDING" : "DONE"

    let qry = "INSERT INTO `transactions_tbl`(branch_id,mop,amount,created_by,status,remarks,patient_id)VALUES(?,?,?,?,?,?,?)"
    const resIns = await conn.executeQuery(qry,[e.branch,e.mop,e.total_amount,e.created_by,trans_status,e.remarks,e.patient_id]).then((res)=>res);
    trans_id = resIns.insertId

    if(paymentMethod.length != 0){
        for await (let payment_method of paymentMethod){
            let payment_method_date = moment(payment_method.payment_date + " " +payment_method.payment_time).format("YYYY-MM-DD HH:mm:ss")
            if(payment_method.amount!=0){
                let insPaymnt = "INSERT INTO `transaction_payments_tbl`(transaction_id,amount,mop,payment_date)VALUES(?,?,?,?)"
                const resinsPaymnt = await conn.executeQuery(insPaymnt,[trans_id,payment_method.amount,payment_method.mop,payment_method_date]).then((res)=>res);
            }
        }
    }

    if(resIns){

        for await(let procedure of procedures){//procedure loop
            let commission_distributed = 0
            let assistants_commission_distributed = 0
            let secretary_commission_distributed = 0
            let company_revenue = procedure.price * procedure.quantity
            let percentage_rate_concat = 0
            let percentage_assist_rate_concat = 0
            let percentage_secre_rate_concat = 0
            let quantity = procedure.quantity
            let amount = procedure.price
            let quantity_condition = false

            //dentist
            let commission_type = procedure.commission_type  
            let commission_rate = procedure.commission_rate
            //assistant
            let assistants_type = procedure.assistants_type  
            let assistants_rate = procedure.assistants_rate
            //secretary
            let secretary_type = procedure.secretary_type  
            let secretary_rate = procedure.secretary_rate

            let procedure_status = procedure.proc_status == "pending" ? 'pending' : 'done'
            // if(e.mop=="PARTIAL PAYMENT"){
            //     procedure_status = "partial-payment"
            // }

            let tmp = await checkDentistCommission(procedure.dentist,procedure.id)

            if(tmp!=""){
                commission_type = tmp.commission_type  
                commission_rate = tmp.commission_rate
            }
            else{
                let withQuantityComs = await getProcedureQtyReq(procedure.quantity,procedure.id)
    
                if(withQuantityComs!=null){
                    commission_rate = withQuantityComs.dentist_rate
                    assistants_rate = withQuantityComs.assistant_rate
                    secretary_rate = withQuantityComs.secretary_rate
    
                    commission_type = withQuantityComs.dentist_type
                    secretary_type = withQuantityComs.secretary_type
                    assistants_type = withQuantityComs.assistant_type
    
                    quantity_condition = true
                }
        
            }

            //Dentist
            if(procedure.dentist !== null){
                if(commission_type == "fixed"){
                    company_revenue = company_revenue - (commission_rate*quantity)
                    commission_distributed = (commission_rate *quantity)
                }
                else{
                    let comm_rate = commission_rate.toString()
                    percentage_rate_concat = "0." + comm_rate.padStart(2,"0")
                    
                    commission_distributed = ((amount * percentage_rate_concat) *quantity)
                    company_revenue = company_revenue - commission_distributed
                    console.log("percentage")

                    console.log("computation: ",percentage_rate_concat,"-",commission_distributed)
                }
                
                
                let insQryDentist = "INSERT INTO `employee_commissions_tbl`(transaction_id,procedure_id,employee_id,amount,covered_date,status)VALUES(?,?,?,?,?,?)"
                const resinsQryDentist = await conn.executeQuery(insQryDentist,[trans_id,procedure.id,procedure.dentist,commission_distributed,procedure.date,procedure_status]).then((res)=>res);
            }


            //Assistants
            if(procedure.assistants.length!=0){
                if(assistants_type=="fixed"){
                    if(quantity_condition){
                        company_revenue = company_revenue - ((assistants_rate * procedure.assistants.length))
                        assistants_commission_distributed = ((assistants_rate * procedure.assistants.length))
                    }
                    else{
                        company_revenue = company_revenue - ((assistants_rate * procedure.assistants.length)*quantity)
                        assistants_commission_distributed = ((assistants_rate * procedure.assistants.length)*quantity)
                    }
                }
                else{

                    let assist_rate = (assistants_rate * procedure.assistants.length)
                    assist_rate = assist_rate.toString()
                    percentage_assist_rate_concat = "0." + assist_rate.padStart(2,"0")
                    
                    if(quantity_condition){
                        assistants_commission_distributed = ((amount * percentage_assist_rate_concat))
                        company_revenue = company_revenue - (assistants_commission_distributed)
                    }
                    else{
                        assistants_commission_distributed = ((amount * percentage_assist_rate_concat)*quantity)
                        company_revenue = company_revenue - (assistants_commission_distributed)
                    }
                }  
            }


            //Secretary

            if(procedure.secretaries.length!=0){
                if(secretary_type=="fixed"){
                    if(quantity_condition){
                        company_revenue = company_revenue - ((secretary_rate * procedure.secretaries.length))
                        secretary_commission_distributed = ((secretary_rate * procedure.secretaries.length))
                    }
                    else{
                        company_revenue = company_revenue - ((secretary_rate * procedure.secretaries.length)*quantity)
                        secretary_commission_distributed = ((secretary_rate * procedure.secretaries.length)*quantity)
                    }
                }
                else{
                    let secre_rate = (secretary_rate * procedure.secretaries.length)
                    secre_rate = secretary_rate.toString()
                    percentage_secre_rate_concat = "0." + secre_rate.padStart(2,"0")
                    if(quantity_condition){
                        secretary_commission_distributed = ((amount * percentage_secre_rate_concat))
                        company_revenue = company_revenue - secretary_commission_distributed
                    }
                    else{
                        secretary_commission_distributed = ((amount * percentage_secre_rate_concat)*quantity)
                        company_revenue = company_revenue - secretary_commission_distributed
                    }
                }
            }

            //distribute assistants commission
            for await(let assistant of procedure.assistants){
                if(assistants_rate>0){
                    let amount_assistant_comms = 0
                    if(quantity_condition){
                        amount_assistant_comms = assistants_rate
                    }
                    else{
                        amount_assistant_comms = assistants_rate*quantity
                    }
                    let insQry = "INSERT INTO assistants_commissions_tbl(transaction_id,assistant_id,procedure_id,amount,covered_date,status)VALUES(?,?,?,?,?,?)"
                    const resInsTransDEtails = await conn.executeQuery(insQry,[trans_id,assistant,procedure.id,amount_assistant_comms,procedure.date,procedure_status]).then((res)=>res);
                }
            }
            

            //distribute secretary commission
            for await(let secretary of procedure.secretaries){
                if(secretary_rate>0){
                    let amount_secretary_comms = 0
                    if(quantity_condition){
                        amount_secretary_comms = secretary_rate
                    }
                    else{
                        amount_secretary_comms = secretary_rate*quantity
                    }
                    let insQry = "INSERT INTO secretaries_commissions_tbl(transaction_id,assistant_id,procedure_id,amount,covered_date,status)VALUES(?,?,?,?,?,?)"
                    const resInsTransDEtails = await conn.executeQuery(insQry,[trans_id,secretary,procedure.id,amount_secretary_comms,procedure.date,procedure_status]).then((res)=>res);
                }
            }            

            let procDateAndTime = moment(procedure.date + " " + procedure.time).format("YYYY-MM-DD HH:MM")
            
            let insTransDetails = "INSERT INTO `transaction_details_tbl`(transaction_id,patient_id,procedure_id,dentist_id,amount,commission_distributed,company_revenue,covered_date,assistants_commission_distributed,secretary_commission_distributed,proc_status,qty,mop)VALUES(?,?,?,IFNULL(?,0),?,?,?,?,?,?,?,?,?)"
            const resInsTransDEtails = await conn.executeQuery(insTransDetails,[trans_id,e.patient_id,procedure.id,procedure.dentist,amount,commission_distributed,company_revenue,procDateAndTime,assistants_commission_distributed,secretary_commission_distributed,procedure_status,procedure.quantity,e.mop]).then((res)=>res);
        }
        return true
    }
    else{
        return false
    }
}

const get = async function(req){
    // let qry = "SELECT * FROM transactions_tbl limit 10"
    
    // const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    return "hello"

    // if(resQry){
    //     const arr = resQry.map(async (item,index) => ({
    //         id                      : item.id,
    //         branch_name             : item.branch_name,
    //         mop                     : item.mop,
    //         amount                  : item.amount,
    //         status                  : item.status,
    //         created_by              : item.created_by,
    //         remarks                 : item.remarks,
    //         // details                 : await getTransactionDetails(item.id),
    //         // payments                : await getSalesPaymentDetails(item.id),
    //         date_created            : moment(item.date_created).format("LLL")

    //     }))

    //     return Promise.all(arr)
    // }
    // else{
    //     return []
    // }
}

/*
const get = async function(req){
    let e = req.body
    let rows = e.rows
    let page = e.page
    let offset = (page-1)*rows
    let branch = e.branch
    let branchQry = ""
    let patientQry = ""
    let dentistQry = ""
    let dateQry = ""
    let remarksQry = ""

    let search_patient = e.search_patient
    let search_dentist = e.search_dentist
    let search_date    = e.search_date
    let search_remarks = e.search_remarks

    if(branch!==null){
        branchQry = " AND tmp.branch_id = " + branch
    }

    if(search_patient!==null){
        if(search_patient!==""){
            patientQry = " AND tmp.patient LIKE '%"+search_patient+"%'"
        }
    }

    if(search_dentist!==null){
        if(search_dentist.length!=0){
            search_dentist = search_dentist.toString()
            
            dentistQry = " AND tmp.dentist_id IN("+search_dentist+")"
        }
    }

    if(search_date!==null){
        if(search_date!==""){
            dateQry = " AND LEFT(tmp.date_created,10) = '"+search_date+"'"
        }
    }

    if(search_remarks!==null){
        if(search_remarks!==""){
            remarksQry = " AND tmp.remarks LIKE '%"+search_remarks+"%'" 
        }
    }

    // let qry = "SELECT * FROM ( SELECT a.id, a.`branch_id`, b.`branch_name`, a.`date_created`, a.`status`, a.mop, a.amount, CONCAT(c.fname, ' ', c.lname) AS created_by, a.remarks, (SELECT UCASE(CONCAT(e.lname,', ',e.fname,' ',e.mname)) FROM `patients_tbl` e WHERE e.id = (SELECT d.patient_id FROM `transaction_details_tbl` d WHERE d.transaction_id = a.id LIMIT 1)) AS patient, (SELECT CONCAT(e.lname,', ',e.fname,' ',e.mname) FROM `dentists_tbl` e WHERE e.id = (SELECT d.dentist_id FROM `transaction_details_tbl` d WHERE d.transaction_id = a.id LIMIT 1)) AS dentist, (SELECT d.dentist_id FROM `transaction_details_tbl` d WHERE d.transaction_id = a.id LIMIT 1) AS dentist_id FROM `transactions_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.`branch_id` INNER JOIN users_tbl c ON c.id = a.created_by) AS tmp WHERE 1=1"+branchQry+" ORDER BY tmp.id DESC LIMIT ? OFFSET ?"

    let qry = "SELECT * FROM transactions_tbl"
    
    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                      : item.id,
            branch_name             : item.branch_name,
            mop                     : item.mop,
            amount                  : item.amount,
            status                  : item.status,
            created_by              : item.created_by,
            remarks                 : item.remarks,
            // details                 : await getTransactionDetails(item.id),
            // payments                : await getSalesPaymentDetails(item.id),
            date_created            : moment(item.date_created).format("LLL")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}
*/

const getSalesPaymentDetails = async function(transactionId){
    let qry = "SELECT id,amount,mop,payment_date,date_created FROM `transaction_payments_tbl` WHERE transaction_id = ?"
    const resQry = await conn.getQuery(qry,[transactionId]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                      : item.id,
            amount                  : item.amount,
            mop                     : item.mop,
            payment_date            : moment(item.payment_date).format("LLL"),
            date_created            : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getTotalSales = async function(req){
    let e = req.body
    let branch = e.branch || null
    let branchQry = ""
    let patientQry = ""
    let dentistQry = ""
    let dateQry = ""
    let remarksQry = ""

    let search_patient = e.search_patient || null
    let search_dentist = e.search_dentist || null
    let search_date    = e.search_date || null
    let search_remarks = e.search_remarks || null


    if(branch!==null){
        branchQry = " AND tmp.branch_id = " + branch
    }

    if(search_patient!==null){
        if(search_patient!==""){
            patientQry = " AND tmp.patient LIKE '%"+search_patient+"%'"
        }
    }

    if(search_dentist!==null){
        if(search_dentist.length!=0){
            search_dentist = search_dentist.toString()
            
            dentistQry = " AND tmp.dentist_id IN("+search_dentist+")"
        }
    }

    if(search_date!==null){
        if(search_date!==""){
            dateQry = " AND LEFT(tmp.date_created,10) = '"+search_date+"'"
        }
    }

    if(search_remarks!==null){
        if(search_remarks!==""){
            remarksQry = " AND tmp.remarks LIKE '%"+search_remarks+"%'" 
        }
    }

   let qry =  "SELECT COUNT(id) AS cnt FROM ( SELECT a.id, a.`branch_id`, b.`branch_name`, a.`amount`, a.mop, a.`status`, a.`remarks`, a.`date_created`, CONCAT(d.lname,', ',d.fname,' ',d.mname) AS patient, CONCAT(c.`lname`, ', ', c.fname) AS created_by FROM transactions_tbl a INNER JOIN `branches_lookup_tbl` b ON a.`branch_id` = b.id INNER JOIN `users_tbl` c ON c.id = a.`created_by` INNER JOIN `patients_tbl` d ON d.id = a.`patient_id` ORDER BY id DESC ) AS tmp WHERE 1=1 "+branchQry+patientQry+dateQry+remarksQry

   console.log(qry)

    const resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const getTransactionDetails = async function(trans_id){

    let qry = "SELECT a.id,UCASE(CONCAT(c.`lname`,', ',c.`fname`, ' ',c.`mname`)) AS patient_name,CONCAT(e.`lname`,', ',e.fname,' ',e.`mname`) AS dentist_name,d.`procedure_name`,a.`amount`,a.mop,a.`status`,a.`date_created`,b.amount as price,f.branch_name FROM `transactions_tbl` a INNER JOIN `transaction_details_tbl` b ON b.`transaction_id` = a.id INNER JOIN patients_tbl c ON c.id = b.`patient_id` INNER JOIN procedures_tbl d ON d.id = b.`procedure_id` LEFT JOIN dentists_tbl e ON e.id = b.`dentist_id` INNER JOIN branches_lookup_tbl f ON f.id = a.branch_id WHERE a.id = ?"
    const resQry = await conn.getQuery(qry,[trans_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                      : item.id,
            branch                  : item.branch_name,
            patient_name            : item.patient_name,
            dentist_name            : item.dentist_name,
            procedure_name          : item.procedure_name,
            amount                  : item.price,
            mop                     : item.mop,
            status                  : item.status,
            date_created            : moment(item.date_created).format("LLL")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const checkDentistCommission = async function(dentist_id,procedure_id){
    console.log("dentist and proc id : ",dentist_id,procedure_id)

    let qry = "SELECT * FROM `rank_commission_benefits_mapping_tbl` WHERE rank_id = (SELECT rank_id FROM `dentists_tbl` WHERE id = ?) AND procedure_id = ?"
    const resQry = await conn.getQuery(qry,[dentist_id,procedure_id]).then((res)=>res);

    
    if(resQry){
        if(resQry.length == 0){
            return ""
        }
        else{
            let tmp = {
                commission_rate : resQry[0].commission_rate,
                commission_type : resQry[0].commission_type
            }
            console.log(tmp)
            return tmp
        }
    }
}

const getProcedureQtyReq = async function(qty,procedure_id){
    console.log(qty,procedure_id)

    let qry = "SELECT * FROM `procedures_qty_req_tbl` WHERE qty=? AND procedure_id=?"
    const resQry = await conn.getQuery(qry,[qty,procedure_id]).then((res)=>res);

    if(resQry){
        console.log(resQry)
        if(resQry.length!=0){
            return resQry[0]
        }
        else{
            return null
        }
    }
}

const voidTransaction = async function(req){
    let e = req.body
    let qry = "DELETE FROM `transactions_tbl` WHERE id = ?"
    const resQry = await conn.executeQuery(qry,[e.id]).then((res)=>res);
    
    if(resQry){
        let qry1 = "DELETE FROM `transaction_details_tbl` WHERE transaction_id = ?"
        const resQry1 = await conn.executeQuery(qry1,[e.id]).then((res)=>res);
        if(resQry1){
            let asst = "DELETE FROM `assistants_commissions_tbl` WHERE transaction_id = ?"
            const resAsst = await conn.executeQuery(asst,[e.id]).then((res)=>res);

            let sec = "DELETE FROM `secretaries_commissions_tbl` WHERE transaction_id = ?"
            const resSec = await conn.executeQuery(sec,[e.id]).then((res)=>res);

            let doc = "DELETE FROM `employee_commissions_tbl` WHERE transaction_id = ?"
            const resDoc = await conn.executeQuery(doc,[e.id]).then((res)=>res);

            let qry = "DELETE FROM `transaction_payments_tbl` WHERE transaction_id = ?"
            const resQry = await conn.executeQuery(qry,[e.id]).then((res)=>res);

            return true
        }
    }
    else{
        return false
    }
}

const getSales = async function(req) {
    let e = req.body
    let rows = e.rows
    let page = e.page
    let offset = (page-1)*rows
    let branch = e.branch || null
    let branchQry = ""
    let patientQry = ""
    let dentistQry = ""
    let dateQry = ""
    let remarksQry = ""

    let search_patient = e.search_patient || null
    let search_dentist = e.search_dentist || null
    let search_date    = e.search_date || null
    let search_remarks = e.search_remarks || null 

    if(branch!==null){
        branchQry = " AND tmp.branch_id = " + branch
    }

    if(search_patient!==null){
        if(search_patient!==""){
            patientQry = " AND tmp.patient LIKE '%"+search_patient+"%'"
        }
    }

    if(search_dentist!==null){
        if(search_dentist.length!=0){
            search_dentist = search_dentist.toString()
            
            dentistQry = " AND tmp.dentist_id IN("+search_dentist+")"
        }
    }

    if(search_date!==null){
        if(search_date!==""){
            dateQry = " AND LEFT(tmp.date_created,10) = '"+search_date+"'"
        }
    }

    if(search_remarks!==null){
        if(search_remarks!==""){
            remarksQry = " AND tmp.remarks LIKE '%"+search_remarks+"%'" 
        }
    }

    // let qry = "SELECT * FROM ( SELECT a.id, a.`branch_id`, b.`branch_name`, a.`date_created`, a.`status`, a.mop, a.amount, CONCAT(c.fname, ' ', c.lname) AS created_by, a.remarks, (SELECT UCASE(CONCAT(e.lname,', ',e.fname,' ',e.mname)) FROM `patients_tbl` e WHERE e.id = (SELECT d.patient_id FROM `transaction_details_tbl` d WHERE d.transaction_id = a.id LIMIT 1)) AS patient, (SELECT CONCAT(e.lname,', ',e.fname,' ',e.mname) FROM `dentists_tbl` e WHERE e.id = (SELECT d.dentist_id FROM `transaction_details_tbl` d WHERE d.transaction_id = a.id LIMIT 1)) AS dentist, (SELECT d.dentist_id FROM `transaction_details_tbl` d WHERE d.transaction_id = a.id LIMIT 1) AS dentist_id FROM `transactions_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.`branch_id` INNER JOIN users_tbl c ON c.id = a.created_by) AS tmp WHERE 1=1"+branchQry+" ORDER BY tmp.id DESC LIMIT ? OFFSET ?"

    let qry = "SELECT * FROM ( SELECT a.id, a.`branch_id`, b.`branch_name`, a.`amount`, a.mop, a.`status`, a.`remarks`, a.`date_created`, CONCAT(d.lname,', ',d.fname,' ',d.mname) AS patient, CONCAT(c.`lname`, ', ', c.fname) AS created_by FROM transactions_tbl a INNER JOIN `branches_lookup_tbl` b ON a.`branch_id` = b.id INNER JOIN `users_tbl` c ON c.id = a.`created_by` INNER JOIN `patients_tbl` d ON d.id = a.`patient_id` ORDER BY id DESC ) AS tmp WHERE 1=1 "+branchQry+patientQry+dateQry+remarksQry+ " ORDER BY tmp.date_created DESC LIMIT ? OFFSET ?"
    
    console.log(qry)
    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                      : item.id,
            branch_name             : item.branch_name,
            patient                 : item.patient,
            mop                     : item.mop,
            amount                  : item.amount,
            status                  : item.status,
            created_by              : item.created_by,
            remarks                 : item.remarks,
            details                 : await getTransactionDetails(item.id),
            payments                : await getSalesPaymentDetails(item.id),
            date_created            : moment(item.date_created).format("LLL")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}



module.exports = {
    save,
    get,
    voidTransaction,
    getTotalSales,
    getSales
}
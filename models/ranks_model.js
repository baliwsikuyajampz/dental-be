var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

const save = async function(req){
    let e = req.body
    let procedure = e.procedure
    let mode = e.mode
    let key = e.key

    if(mode=="add"){
        let qry = "INSERT INTO `ranks_tbl`(rank_name,is_active)VALUES(?,?)"
        const resQry = await conn.executeQuery(qry,[e.rank_name,e.is_active]).then((res)=>res);
        if(resQry){
            let rankId = resQry.insertId
    
            for await(let data of procedure){
                let insQry = "INSERT INTO `rank_commission_benefits_mapping_tbl`(rank_id,procedure_id,commission_rate,commission_type)VALUES(?,?,?,?)"
                const resinsQry = await conn.executeQuery(insQry,[rankId,data.id,data.commission_rate,data.commission_type]).then((res)=>res);
            }
    
            return true
        }
        else{
            return false
        }
    }
    else{
        let qry = "UPDATE ranks_tbl SET rank_name = ?,is_active=? WHERE id = ?"
        const resQry = await conn.executeQuery(qry,[e.rank_name,e.is_active,key]).then((res)=>res);
        if(resQry){
            let rankId = key

            let del = "DELETE FROM `rank_commission_benefits_mapping_tbl` WHERE rank_id = ?"
            const resDel = await conn.executeQuery(del,[rankId]).then((res)=>res);

            for await(let data of procedure){
                let insQry = "INSERT INTO `rank_commission_benefits_mapping_tbl`(rank_id,procedure_id,commission_rate,commission_type)VALUES(?,?,?,?)"
                const resinsQry = await conn.executeQuery(insQry,[rankId,data.id,data.commission_rate,data.commission_type]).then((res)=>res);
            }
    
            return true
        }
        else{
            return false
        }
    }
}

const get = async function(req){
    let e = req.body
    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows

    let qry = "SELECT id,rank_name,is_active,date_created FROM `ranks_tbl` LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                       : item.id,
            rank_name                : item.rank_name,
            is_active                : item.is_active,
            procedure                : await getProceduresByRank(item.id),
            date_created             : moment(item.date_created).format("LLL")
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getProceduresByRank = async function(rankId){
    let qry = "SELECT a.id,a.`rank_id`,a.`procedure_id`,b.`procedure_name`,a.`commission_rate`,a.`commission_type` FROM `rank_commission_benefits_mapping_tbl` a INNER JOIN `procedures_tbl` b ON b.id = a.procedure_id WHERE rank_id = ?"

    const resQry = await conn.getQuery(qry,[rankId]).then((res)=>res);

    if(resQry){

        const arr = resQry.map(async (item,index) => ({
            id                             : item.id,
            rank_id                        : item.rank_id,
            procedure_id                   : item.procedure_id,
            procedure_name                 : item.procedure_name,
            commission_rate                : item.commission_rate,
            commission_type                : item.commission_type,
        }))

        return Promise.all(arr) 
    }
    else{
        return []
    }
}

const getTotal = async function(req){
    let e = req.body

    let qry = "SELECT COUNT(id) AS cnt FROM `ranks_tbl`"
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}


module.exports = {
    save,
    get,
    getTotal
}

var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

const get = async function(req){
    let e = req.body
    let row = e.row
    let page = e.page
    let offset = (page-1)*row
    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " WHERE CONCAT(a.lname,',',a.fname,' ',a.mname) LIKE '%"+search+"%' "
    }
    let qry = "SELECT a.id,a.fname,a.mname,a.lname,a.branch_id,a.rank_id,a.date_created,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname,b.`rank_name`,c.`branch_name`,a.is_active,CONCAT(d.fname,' ',d.lname) AS created_by FROM `dentists_tbl` a LEFT JOIN `ranks_tbl` b ON b.id = a.`rank_id` INNER JOIN branches_lookup_tbl c ON c.id = a.`branch_id` LEFT JOIN users_tbl d ON d.id = a.created_by "+searchQry+" ORDER BY a.rank_id LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[row,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            branch_id       : item.branch_id,
            rank_id         : item.rank_id,
            fullname        : item.fullname,
            rank_name       : item.rank_name,
            branch_name     : item.branch_name,
            is_active       : item.is_active,
            created_by      : item.created_by,
            date_created    : moment(item.date_created).format("LLL")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getDoctorsTotalCount = async function(req){
    let e = req.body
    let search = e.search
    let searchQry = ""

    if(search!=""){
        searchQry = " WHERE CONCAT(lname,',',fname,' ',mname) LIKE '%"+search+"%' "
    }

    let qry = "SELECT COUNT(id) as cnt FROM dentists_tbl" +searchQry
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }

}

const getDoctorCommissionById = async function(req){
    let e = req.body

    let qry = "SELECT DISTINCT a.id,a.`transaction_id`,a.`amount`,a.`covered_date`,b.`procedure_name`,CONCAT(d.lname,', ',d.fname,' ') AS patient_name FROM `employee_commissions_tbl` a INNER JOIN `procedures_tbl` b ON b.id = a.`procedure_id` INNER JOIN `transaction_details_tbl` c ON c.`transaction_id` = a.`transaction_id` INNER JOIN `patients_tbl` d ON d.id = c.`patient_id`  WHERE a.employee_id = ? ORDER  BY a.id DESC LIMIT 10"
    const resQry = await conn.getQuery(qry,[e.key]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                       : item.id,
            transaction_id           : item.transaction_id,
            amount                   : item.amount,
            covered_date             : moment(item.covered_date).format("LLL"),
            procedure_name           : item.procedure_name,
            patient_name             : item.patient_name,

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const save = async function(req){
    let e = req.body
    let mode = e.mode

    if(mode=="add"){
        let qry = "INSERT INTO `dentists_tbl`(fname,mname,lname,branch_id,created_by,is_active,rank_id)VALUES(?,?,?,?,?,?,?)"
        const resQry = await conn.executeQuery(qry,[e.fname,e.mname,e.lname,e.branch,e.created_by,e.is_active,e.rank]).then((res)=>res);
        if(resQry){
            return true
        }
        else{
            return false
        }
    }
    else{
        let qry = "UPDATE dentists_tbl SET fname=?,mname=?,lname=?,branch_id=?,is_active=?,rank_id = ? WHERE id = ?"
        const resQry = await conn.executeQuery(qry,[e.fname,e.mname,e.lname,e.branch,e.is_active,e.rank,e.key]).then((res)=>res);
        if(resQry){
            return true
        }
        else{
            return false
        }
    }

}

const getDoctorDetailsById = async function(req){
    let e = req.body

    let qry = "SELECT a.id,b.`branch_name`,c.`rank_name`,a.`fname`,a.`mname`,a.`lname`,IFNULL(a.contact_no,'N/A') as contact_no,IFNULL(a.email,'N/A') as email,CONCAT(a.`lname`,', ',a.fname,' ',a.`mname`) AS fullname FROM `dentists_tbl` a INNER JOIN branches_lookup_tbl b ON b.id = a.`branch_id` INNER JOIN `ranks_tbl` c ON c.`id` = a.`rank_id` WHERE a.id = ?"
    const resQry = await conn.findFirst(qry,[e.id]).then((res)=>res);

    return resQry
}

module.exports = {
    get,
    getDoctorCommissionById,
    save,
    getDoctorsTotalCount,
    getDoctorDetailsById
}
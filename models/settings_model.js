var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd


//#region BRANCHES

const getTeethStructure = async function(req){

    let qry = "SELECT * FROM teeth_chart_tbl"
    const resQry = await conn.getQuery(qry,[]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                  : item.id,
            tooth_number        : item.tooth_number,
            tooth_part          : item.tooth_part,
            img                 : item.img,

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

module.exports = {
    getTeethStructure

}
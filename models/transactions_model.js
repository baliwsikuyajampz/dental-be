var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

const getPendingTransactions = async function(req){
    let e = req.body

    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let branch = e.branch || null
    let branchQry = ""
    if(branch!==null){
        branchQry = " AND e.branch_id = " + branch
    }

    let qry = "SELECT a.id,f.`branch_name`,a.`amount`,CONCAT(b.`lname`,', ',b.fname ,' ',b.`mname`) AS patient_name,a.procedure_id,a.transaction_id,c.`procedure_name`,IFNULL(CONCAT(d.`lname`,', ',d.`fname`,' ',d.`mname`),'') AS dentist_name,a.`covered_date`,a.`proc_status` FROM `transaction_details_tbl` a INNER JOIN `patients_tbl` b ON a.`patient_id` = b.id INNER JOIN `procedures_tbl` c ON c.id = a.`procedure_id` LEFT JOIN `dentists_tbl` d ON d.id = a.`dentist_id` INNER JOIN `transactions_tbl` e ON e.id = a.`transaction_id` INNER JOIN `branches_lookup_tbl` f ON f.id = e.`branch_id` WHERE a.proc_status <> 'done'"+branchQry+" ORDER BY a.id DESC LIMIT ? OFFSET ?"
    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                  : item.id,
            branch_name         : item.branch_name,
            amount              : item.amount,
            patient_name        : item.patient_name,
            procedure_id        : item.procedure_id,
            transaction_id        : item.transaction_id,
            procedure_name        : item.procedure_name,
            dentist_name        : item.dentist_name,
            covered_date        : moment(item.covered_date).format("YYYY-MM-DDTHH:mm"),
            proc_status        : item.proc_status,
            covered_date_formatted        : moment(item.covered_date).format("MMM DD,  YYYY hh:mm a")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const updatePendingTransaction = async function(req){
    let e = req.body
    let qry = ""

    if(e.proc_status == 'pending'){
        qry = "UPDATE `transaction_details_tbl` SET proc_status = 'done',covered_date = NOW() WHERE id = ?"
    }
    else{
        qry = "UPDATE `transaction_details_tbl` SET proc_status = 'done' WHERE id = ?"
    }

    const resQry = await conn.executeQuery(qry,[e.id]).then((res)=>res);
    if(resQry){
        //update doctor commission

        let updDentist = "UPDATE `employee_commissions_tbl` SET STATUS = 'done', covered_date = NOW() WHERE transaction_id = ? AND procedure_id = ?"
        const resupdDentist = await conn.executeQuery(updDentist,[e.transaction_id,e.procedure_id]).then((res)=>res);

        //update assistant commission
        let updAsst = "UPDATE `assistants_commissions_tbl` SET STATUS = 'done', covered_date = NOW() WHERE transaction_id = ? AND procedure_id = ?"
        const resupdAsst = await conn.executeQuery(updAsst,[e.transaction_id,e.procedure_id]).then((res)=>res);

        let updSec = "UPDATE `secretaries_commissions_tbl` SET STATUS = 'done', covered_date = NOW() WHERE transaction_id = ? AND procedure_id = ?"
        const resupdSec = await conn.executeQuery(updSec,[e.transaction_id,e.procedure_id]).then((res)=>res);

        return true
    }
    else{
        return false
    }
}

const getPendingTransactionTotal = async function(req){
    let e = req.body
    let branch = e.branch || null
    let branchQry = ""
    if(branch!==null){
        branchQry = " AND e.branch_id = " + branch
    }

    let qry = "SELECT COUNT(a.id) as cnt FROM `transaction_details_tbl` a INNER JOIN `patients_tbl` b ON a.`patient_id` = b.id INNER JOIN `procedures_tbl` c ON c.id = a.`procedure_id` LEFT JOIN `dentists_tbl` d ON d.id = a.`dentist_id` INNER JOIN `transactions_tbl` e ON e.id = a.`transaction_id` INNER JOIN `branches_lookup_tbl` f ON f.id = e.`branch_id` WHERE a.proc_status <> 'done'"+branchQry+" ORDER BY a.id DESC LIMIT 10"

    const res = await conn.findFirst(qry,[]).then((res)=>res);

    if(res){
        return res.cnt
    }
    else{
        return 0
    }

}

const updateSchedule = async function(req){
    let e = req.body

    let qry = "UPDATE transaction_details_tbl SET covered_date = ? WHERE id = ?"
    const res = await conn.executeQuery(qry,[e.date,e.key]).then((res)=>res);
    if(res){
        return true
    }
    else{
        return false
    }

}

const getPendingPayments = async function(req){
    let e = req.body

    let page = e.page
    let rows = e.rows
    let offset = (page-1)*rows
    let branch = e.branch || null
    let branchQry = ""
    if(branch!==null){
        branchQry = " AND a.branch_id = " + branch
    }

    let qry = "SELECT a.id,a.`branch_id`,d.`branch_name`,a.`mop`,a.`amount` AS total_amount,a.`status`,a.`date_created`,CONCAT(b.`fname`,' ',b.lname) AS created_by,(SELECT SUM(c.amount) FROM `transaction_payments_tbl` c WHERE c.transaction_id = a.id) AS total_payment FROM `transactions_tbl` a INNER JOIN `users_tbl` b ON b.id = a.`created_by` INNER JOIN `branches_lookup_tbl` d ON d.id = a.`branch_id` WHERE `status` = 'PENDING' "+branchQry+" LIMIT ? OFFSET ?"
    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                  : item.id,
            branch_name         : item.branch_name,
            total_amount        : item.total_amount,
            total_payment       : item.total_payment,
            remaining_bal       : item.total_amount - item.total_payment,
            created_by          : item.created_by,
            payment_details     : await getPendingPaymentDetails(item.id),
            transaction_details : await getPendingPaymentTransactionDetails(item.id),
            date_created        : moment(item.date_created).format("LLL"),

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getPendingPaymentsTotal = async function(req){
    let e = req.body

    let branch = e.branch || null
    let branchQry = ""
    if(branch!==null){
        branchQry = " AND a.branch_id = " + branch
    }

    let qry = "SELECT COUNT(a.id) AS cnt FROM `transactions_tbl` a INNER JOIN `users_tbl` b ON b.id = a.`created_by` INNER JOIN `branches_lookup_tbl` d ON d.id = a.`branch_id` WHERE `status` = 'PENDING' "+branchQry
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);

    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }
}

const getPendingPaymentDetails = async function(trans_id){
    let qry = "SELECT a.id,a.`transaction_id`,a.`amount`,a.`mop`,a.`payment_date`,a.`date_created` FROM `transaction_payments_tbl` a WHERE a.transaction_id = ?"
    const resQry = await conn.getQuery(qry,[trans_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                  : item.id,
            transaction_id      : item.transaction_id,
            amount              : item.amount,
            mop                 : item.mop,
            payment_date        : moment(item.payment_date).format("LLL"),

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const getPendingPaymentTransactionDetails = async function(trans_id){
    let qry = "SELECT a.id,CONCAT(b.lname,', ',b.fname,' ',b.mname) AS patient_name,CONCAT(d.lname,', ',d.fname,' ',d.mname) AS dentist_name,c.procedure_name,a.qty,a.amount FROM `transaction_details_tbl` a INNER JOIN `patients_tbl` b ON b.id = a.`patient_id` INNER JOIN `procedures_tbl` c ON c.id = a.`procedure_id` LEFT JOIN `dentists_tbl` d ON d.id = a.`dentist_id` WHERE a.transaction_id = ?"
    const resQry = await conn.getQuery(qry,[trans_id]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id                  : item.id,
            patient_name        : item.patient_name,
            dentist_name        : item.dentist_name,
            procedure_name      : item.procedure_name,
            qty                 : item.qty,
            amount              : item.amount,
            total_amount        : item.amount * item.qty,

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }
}

const addPayment = async function(req){
    let e = req.body

    let qry = "INSERT INTO `transaction_payments_tbl`(transaction_id,amount,mop,payment_date)VALUES(?,?,?,?)"
    const resQry = await conn.executeQuery(qry,[e.transaction_id,e.amount,e.mop,e.date]).then((res)=>res);

    if(resQry){
        
        if(e.remaining_bal == e.amount){
            let upd = "UPDATE `transactions_tbl` SET `status` = 'DONE' WHERE id = ? "
            const resUpd = await conn.executeQuery(upd,[e.transaction_id]).then((res)=>res);

            if(resUpd){
                return true
            }
            else{
                return false
            }
        }
        else{
            return true
        } 
    }
    else{
        return false
    }
}

module.exports = {
    getPendingTransactions,
    updatePendingTransaction,
    getPendingTransactionTotal,
    updateSchedule,
    getPendingPayments,
    getPendingPaymentsTotal,
    addPayment

}
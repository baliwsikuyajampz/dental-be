var conn = require("../services/mysql/mysql_service")
var base = require("../services/base_service")
var moment = require("moment")
var config = require("../config/config")
var backEndUrl = config.URLs.backEnd

const save = async function(req){
    let e = req.body

    if(e.mode == "add"){
        let qry = "INSERT INTO `secretaries_tbl`(branch_id,fname,mname,lname,is_active,created_by)VALUES(?,?,?,?,?,?)"
        const resQry = await conn.executeQuery(qry,[e.branch,e.fname,e.mname,e.lname,e.is_active,e.created_by]).then((res)=>res);
    
        if(resQry){
            return true
        }
        else{
            false
        }
    }
    else{
        let qry = "UPDATE secretaries_tbl SET branch_id=?,fname=?,mname=?,lname=?,is_active=? WHERE id = ?"
        const resQry = await conn.executeQuery(qry,[e.branch,e.fname,e.mname,e.lname,e.is_active,e.key]).then((res)=>res);
    
        if(resQry){
            return true
        }
        else{
            false
        }
    }

}

const get = async function(req){
    let e = req.body

    let page = e.page
    let rows = e.rows
    let offset = (page-1) * rows

    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " WHERE CONCAT(a.lname,' ',a.fname,' ',a.mname) LIKE '%"+search+"%' "
    }

    let qry = "SELECT a.id,a.`branch_id`,a.fname,a.mname,a.lname,a.`date_created`,a.`is_active`,CONCAT(a.lname,', ',a.fname,' ',a.mname) AS fullname,b.`branch_name`,CONCAT(c.fname,' ',c.lname) AS created_by FROM `secretaries_tbl` a INNER JOIN `branches_lookup_tbl` b ON b.id = a.`branch_id` LEFT JOIN users_tbl c ON c.id = a.created_by  "+searchQry+" LIMIT ? OFFSET ?"

    const resQry = await conn.getQuery(qry,[rows,offset]).then((res)=>res);

    if(resQry){
        const arr = resQry.map(async (item,index) => ({
            id              : item.id,
            fname           : item.fname,
            mname           : item.mname,
            lname           : item.lname,
            branch_id       : item.branch_id,
            fullname        : item.fullname,
            branch_name     : item.branch_name,
            is_active       : item.is_active,
            created_by      : item.created_by,
            date_created    : moment(item.date_created).format("LLL")

        }))

        return Promise.all(arr)
    }
    else{
        return []
    }

}

const getTotal = async function(req){
    let e = req.body

    let search = e.search
    let searchQry = ""
    if(search!=""){
        searchQry = " WHERE CONCAT(lname,' ',fname,' ',mname) LIKE '%"+search+"%' "
    }


    let qry = "SELECT COUNT(id) as cnt FROM secretaries_tbl " + searchQry
    const resQry = await conn.findFirst(qry,[]).then((res)=>res);
    if(resQry){
        return resQry.cnt
    }
    else{
        return 0
    }

}

module.exports = {
    save,
    get,
    getTotal
}
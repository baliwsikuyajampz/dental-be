/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 5.7.44-log : Database - patient_info_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`patient_info_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `patient_info_db`;

/*Table structure for table `assistants_commissions_tbl` */

DROP TABLE IF EXISTS `assistants_commissions_tbl`;

CREATE TABLE `assistants_commissions_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) DEFAULT NULL,
  `assistant_id` bigint(20) DEFAULT NULL,
  `procedure_id` bigint(20) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `status` enum('done','pending') DEFAULT 'done',
  `covered_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `assistants_commissions_tbl` */

insert  into `assistants_commissions_tbl`(`id`,`transaction_id`,`assistant_id`,`procedure_id`,`amount`,`status`,`covered_date`,`date_created`) values 
(1,1,1,66,100.00,'done','2024-05-28 05:17:00','2024-05-28 05:17:54'),
(2,2,1,66,200.00,'done','2024-05-28 05:18:00','2024-05-28 05:18:41'),
(3,3,1,25,100.00,'done','2024-05-29 00:00:00','2024-05-29 06:26:35'),
(4,4,1,25,200.00,'done','2024-05-29 00:00:00','2024-05-29 06:28:37'),
(5,5,1,25,100.00,'done','2024-05-29 00:00:00','2024-05-29 06:31:19'),
(6,7,1,25,200.00,'done','2024-05-29 00:00:00','2024-05-29 06:37:53'),
(7,10,3,15,20.00,'done','2024-06-02 11:38:00','2024-06-02 11:38:11'),
(8,11,3,15,40.00,'done','2024-06-02 00:00:00','2024-06-02 11:40:16');

/*Table structure for table `assistants_tbl` */

DROP TABLE IF EXISTS `assistants_tbl`;

CREATE TABLE `assistants_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `assistants_tbl` */

insert  into `assistants_tbl`(`id`,`branch_id`,`fname`,`mname`,`lname`,`is_active`,`created_by`,`date_created`) values 
(1,1,'a','a','a',1,1,'2024-05-06 04:29:14'),
(2,3,'Angelica','','Nafarrete',1,1,'2024-05-31 03:29:02'),
(3,3,'Roda','','Garcia',1,1,'2024-05-31 03:29:13'),
(4,3,'Rayzle Lou','','Hayag',1,1,'2024-05-31 03:29:22'),
(5,3,'Krizzle','','Sibayan',1,1,'2024-05-31 03:29:32'),
(6,3,'Anne Zenith','','Mulles',1,1,'2024-05-31 03:29:44'),
(7,3,'Marivic','','Asoy',1,1,'2024-05-31 03:29:53'),
(8,2,'Rica Marie','','Ruiz',1,1,'2024-05-31 03:30:05'),
(9,1,'Krizzia','','Lacuna',1,1,'2024-05-31 03:31:07'),
(10,1,'Precious Ann','','Maca',1,1,'2024-05-31 03:31:51'),
(11,1,'Ina Joy','','Francisco',1,1,'2024-05-31 03:32:04'),
(12,3,'Chona','','Pacson',1,1,'2024-05-31 03:33:21'),
(13,1,'fsdafdsaf','','sdafsdafsa',1,1,'2024-06-03 08:14:01');

/*Table structure for table `branches_lookup_tbl` */

DROP TABLE IF EXISTS `branches_lookup_tbl`;

CREATE TABLE `branches_lookup_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_code` varchar(100) DEFAULT NULL,
  `branch_name` varchar(200) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `address` text,
  `contact_no` varchar(20) DEFAULT NULL,
  `is_main` enum('0','1') DEFAULT '0',
  `is_active` enum('0','1') DEFAULT '1',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `branches_lookup_tbl` */

insert  into `branches_lookup_tbl`(`id`,`branch_code`,`branch_name`,`location`,`address`,`contact_no`,`is_main`,`is_active`,`date_created`) values 
(1,'makati','Makati',NULL,NULL,NULL,'0','1','2024-02-18 18:50:34'),
(2,'molino','Molino',NULL,NULL,NULL,'0','1','2024-03-01 14:53:58'),
(3,'gentrias','General Trias',NULL,NULL,NULL,'0','1','2024-03-01 14:54:04');

/*Table structure for table `dentists_tbl` */

DROP TABLE IF EXISTS `dentists_tbl`;

CREATE TABLE `dentists_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) DEFAULT NULL,
  `rank_id` bigint(20) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `contact_no` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `dentists_tbl` */

insert  into `dentists_tbl`(`id`,`branch_id`,`rank_id`,`fname`,`mname`,`lname`,`contact_no`,`email`,`is_active`,`created_by`,`date_created`) values 
(1,1,1,'Daisy','','Sitjar','0922222222','test@email.com','1',1,'2024-04-29 00:27:04'),
(2,1,3,'Janine','','Isip',NULL,NULL,'1',1,'2024-04-29 00:27:14'),
(3,1,3,'Janine Kyla','','Hernandez',NULL,NULL,'1',1,'2024-04-29 00:27:28'),
(4,1,3,'Neal Austeen','','Adriano',NULL,NULL,'1',1,'2024-04-29 00:27:42'),
(5,1,3,'Maria Selyn','','Llado',NULL,NULL,'1',1,'2024-04-29 00:27:56'),
(6,1,2,'Mary Gabrielle','','Dayag',NULL,NULL,'1',1,'2024-04-29 00:28:08'),
(7,1,2,'Viola Jane','','Ballos',NULL,NULL,'1',1,'2024-04-29 00:29:20'),
(8,1,3,'Marivette Nicole','','Llamas',NULL,NULL,'1',1,'2024-04-29 00:29:32'),
(9,1,3,'Raina Katrina','','Du',NULL,NULL,'1',1,'2024-04-29 00:29:59'),
(10,1,3,'Katrina','','Fernando',NULL,NULL,'1',1,'2024-04-29 00:30:07'),
(11,1,NULL,'fdsafdsa','','fdsafsda',NULL,NULL,'1',1,'2024-06-03 08:11:03');

/*Table structure for table `employee_commissions_tbl` */

DROP TABLE IF EXISTS `employee_commissions_tbl`;

CREATE TABLE `employee_commissions_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) NOT NULL,
  `procedure_id` bigint(20) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `status` enum('done','pending') DEFAULT 'done',
  `covered_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `employee_commissions_tbl` */

insert  into `employee_commissions_tbl`(`id`,`transaction_id`,`employee_id`,`procedure_id`,`amount`,`status`,`covered_date`,`date_created`) values 
(1,1,4,66,800.00,'done','2024-05-28 05:17:00','2024-05-28 05:17:54'),
(2,2,7,66,1000.00,'done','2024-05-28 05:18:00','2024-05-28 05:18:41'),
(3,3,8,25,25000.00,'done','2024-05-29 00:00:00','2024-05-29 06:26:35'),
(4,4,1,25,25000.00,'done','2024-05-29 00:00:00','2024-05-29 06:28:37'),
(5,5,6,25,15000.00,'done','2024-05-29 00:00:00','2024-05-29 06:31:19'),
(6,6,4,49,1600.00,'done','2024-05-29 00:00:00','2024-05-29 06:35:09'),
(7,7,2,25,25000.00,'done','2024-05-29 00:00:00','2024-05-29 06:37:53'),
(8,8,4,2,100.00,'done','2024-06-01 05:47:35','2024-06-01 01:50:35'),
(9,9,4,2,100.00,'pending','2024-06-03 06:05:00','2024-06-01 06:05:55'),
(10,10,6,15,4500.00,'done','2024-06-02 11:38:00','2024-06-02 11:38:11'),
(11,11,7,15,9000.00,'done','2024-06-02 00:00:00','2024-06-02 11:40:16'),
(12,12,4,2,1000.00,'done','2024-06-03 08:29:00','2024-06-03 08:29:10'),
(13,13,4,2,100.00,'done','2024-06-03 08:30:00','2024-06-03 08:30:30');

/*Table structure for table `expenses_category_tbl` */

DROP TABLE IF EXISTS `expenses_category_tbl`;

CREATE TABLE `expenses_category_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `expense_category_name` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `expenses_category_tbl` */

insert  into `expenses_category_tbl`(`id`,`expense_category_name`,`is_active`,`created_by`,`date_created`) values 
(1,'Electricity',1,1,'2024-04-22 01:41:50'),
(2,'Rentals',1,1,'2024-04-22 01:41:50'),
(3,'Water',1,1,'2024-04-22 01:41:50'),
(4,'Marketing',1,1,'2024-04-22 01:41:50'),
(5,'Supplies',1,1,'2024-04-22 01:41:50'),
(6,'Accountant and BIR',1,1,'2024-04-22 01:41:50'),
(7,'Government Contribution',1,1,'2024-04-22 01:41:50'),
(8,'Staff',1,1,'2024-04-22 01:41:50'),
(9,'Others',1,1,'2024-04-22 01:41:50'),
(10,'aaaa',1,1,'2024-06-03 08:23:52'),
(11,'Manpower',1,1,'2024-06-07 06:06:09');

/*Table structure for table `expenses_tbl` */

DROP TABLE IF EXISTS `expenses_tbl`;

CREATE TABLE `expenses_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) DEFAULT NULL,
  `expense_category_id` bigint(20) NOT NULL,
  `expense_name` varchar(100) DEFAULT NULL,
  `amount` double(10,2) DEFAULT '0.00',
  `is_company_expense` tinyint(1) DEFAULT '0',
  `covered_date` datetime DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `expenses_tbl` */

insert  into `expenses_tbl`(`id`,`branch_id`,`expense_category_id`,`expense_name`,`amount`,`is_company_expense`,`covered_date`,`is_active`,`created_by`,`date_created`) values 
(1,1,6,'sdafs',500.00,1,'2024-04-16 00:00:00','1',1,'2024-04-21 05:48:11'),
(2,1,6,'sdafs',1500.00,1,'2024-04-01 00:00:00','1',1,'2024-04-21 05:48:37'),
(3,2,1,'fsdgdf',4200.00,0,'2024-04-01 00:00:00','1',1,'2024-04-22 00:31:56'),
(4,1,7,'asdasd',500.00,1,'2024-07-01 00:00:00','1',1,'2024-04-22 00:37:42'),
(5,2,1,'fsdgdf',1000.00,0,'2024-04-01 00:00:00','1',1,'2024-04-22 00:42:21'),
(6,1,4,'asdas',1000.00,0,'2024-07-01 00:00:00','1',1,'2024-04-22 00:46:11'),
(7,1,9,'sdfasd',5000.00,1,'2024-04-01 00:00:00','1',1,'2024-04-22 00:46:47'),
(8,1,6,'sdafs',500.00,0,'2024-04-01 00:00:00','1',1,'2024-04-22 00:46:55'),
(9,1,1,'sadfdsaf',500.00,1,'2024-11-01 00:00:00','1',1,'2024-04-22 00:50:28'),
(10,1,1,'',5000.00,1,'2024-04-01 00:00:00','1',1,'2024-04-26 16:16:22'),
(11,1,1,'electricity',5000.00,1,'2024-05-03 00:00:00','1',1,'2024-05-03 03:57:25'),
(12,1,1,'sdafsda',5000.00,0,'2024-05-15 00:00:00','1',1,'2024-05-06 05:56:43'),
(13,1,2,'rental',200.00,1,'2024-05-09 00:00:00','1',1,'2024-05-09 00:06:18'),
(14,2,1,'Electricity',5000.00,1,'2024-05-09 00:00:00','1',1,'2024-05-09 01:40:17'),
(15,1,3,'water',1500.00,1,'2024-05-16 00:00:00','1',1,'2024-05-09 03:53:53'),
(16,3,1,NULL,5000.00,0,'2024-05-17 00:00:00','1',1,'2024-05-17 17:09:39'),
(17,1,6,'asdasdas',500.00,1,'2024-06-03 00:00:00','1',1,'2024-06-03 00:15:40'),
(18,1,6,'sdafdsafdsafdsa',4500.00,1,'2024-06-03 00:00:00','1',1,'2024-06-03 08:25:49'),
(19,1,10,'asdasdas',1000.00,0,'2024-06-04 00:00:00','1',2,'2024-06-04 23:54:00');

/*Table structure for table `mode_of_payment_tbl` */

DROP TABLE IF EXISTS `mode_of_payment_tbl`;

CREATE TABLE `mode_of_payment_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mop` varchar(150) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mode_of_payment_tbl` */

insert  into `mode_of_payment_tbl`(`id`,`mop`,`is_active`,`date_created`) values 
(1,'CASH','1','2024-04-04 02:58:26'),
(2,'GCASH','1','2024-04-04 02:58:33');

/*Table structure for table `patient_info_tbl` */

DROP TABLE IF EXISTS `patient_info_tbl`;

CREATE TABLE `patient_info_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `patient_id` bigint(20) NOT NULL,
  `nickname` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `contact_no` varchar(100) DEFAULT NULL,
  `office_no` varchar(100) DEFAULT NULL,
  `address` text,
  `occupation` varchar(200) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `religion` varchar(200) DEFAULT NULL,
  `nationality` varchar(200) DEFAULT NULL,
  `guardian_name` varchar(200) DEFAULT NULL,
  `guardian_occupation` varchar(100) DEFAULT NULL,
  `referal_name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `patient_info_tbl` */

insert  into `patient_info_tbl`(`id`,`patient_id`,`nickname`,`email`,`birthday`,`contact_no`,`office_no`,`address`,`occupation`,`gender`,`religion`,`nationality`,`guardian_name`,`guardian_occupation`,`referal_name`) values 
(1,1,NULL,'sdafds','2024-04-17 02:46:28','123213','12312321','safsdafsdafdsaf sdaf sda fasd','sdafds','Female','afsdafdsa','dsfafdsa','asdas','dasdas','da'),
(2,2,NULL,'asdasds','2024-04-18 02:46:31','1231',NULL,'sdasdadas',NULL,'Female',NULL,NULL,'',NULL,NULL),
(3,3,NULL,'sadsadsadsadsa','2024-04-19 02:46:34','12321',NULL,'sdasdasdasdsa',NULL,'Female',NULL,NULL,NULL,NULL,NULL),
(4,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Female',NULL,NULL,NULL,NULL,NULL),
(5,5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Female',NULL,NULL,NULL,NULL,NULL),
(6,6,NULL,NULL,'1986-03-11 00:00:00',NULL,NULL,NULL,NULL,'Female',NULL,NULL,NULL,NULL,NULL),
(7,7,NULL,NULL,'2000-05-15 00:00:00',NULL,NULL,'asdasdas',NULL,'Male',NULL,NULL,NULL,NULL,NULL),
(8,10,NULL,NULL,'2024-05-22 00:00:00',NULL,'','dsafdsafsd','','Male','','',NULL,NULL,NULL),
(9,11,NULL,NULL,'1999-12-17 00:00:00',NULL,'','Imus Cavite','','Female','','',NULL,NULL,NULL),
(10,12,NULL,NULL,'2024-06-03 00:00:00',NULL,'','sdafdsa','','Male','','',NULL,NULL,NULL),
(11,13,NULL,NULL,'2024-06-12 00:00:00',NULL,'','afdsafsda','','Male','','',NULL,NULL,NULL),
(12,14,NULL,NULL,'2024-06-20 00:00:00',NULL,'','sdafsdafdsa','','Male','','',NULL,NULL,NULL),
(13,15,NULL,NULL,'2024-06-21 00:00:00',NULL,'','sdafsdafdsa','','Male','','',NULL,NULL,NULL);

/*Table structure for table `patients_tbl` */

DROP TABLE IF EXISTS `patients_tbl`;

CREATE TABLE `patients_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) DEFAULT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `patients_tbl` */

insert  into `patients_tbl`(`id`,`fname`,`mname`,`lname`,`created_by`,`date_created`) values 
(1,'a','a','a',1,'2024-04-26 02:46:20'),
(2,'b','b','b',1,'2024-04-26 02:46:22'),
(3,'c','c','c',1,'2024-04-26 02:46:24'),
(4,'David','','Smith',1,'2024-05-09 03:11:00'),
(5,'Juan','Santos','Dela Cruz',1,'2024-05-09 03:18:02'),
(6,'Juana','','Dela Pena',1,'2024-05-09 03:22:09'),
(7,'Test','','test',1,'2024-05-09 03:35:51'),
(10,'sadfdsaf','','dsafsda',1,'2024-05-09 15:09:24'),
(11,'Mary Cris','','Largo',1,'2024-05-28 08:22:30'),
(12,'sdaf','','sdafdsafs',1,'2024-06-03 08:05:29'),
(13,'sdfa','','dsafsdafsd',1,'2024-06-03 08:06:21'),
(14,'afsdafdsafds','','afdsafsdafsd',1,'2024-06-03 08:07:29'),
(15,'afsdafdsafdsxczvcxzv','','afdsafsdafsdxczvxczvxc',1,'2024-06-03 08:08:08');

/*Table structure for table `payroll_cutoff_dates_tbl` */

DROP TABLE IF EXISTS `payroll_cutoff_dates_tbl`;

CREATE TABLE `payroll_cutoff_dates_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `payroll_cutoff_dates_tbl` */

/*Table structure for table `procedures_qty_req_tbl` */

DROP TABLE IF EXISTS `procedures_qty_req_tbl`;

CREATE TABLE `procedures_qty_req_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `procedure_id` bigint(20) NOT NULL,
  `qty` int(11) DEFAULT '0',
  `dentist_rate` double(10,2) DEFAULT '0.00',
  `dentist_type` enum('percentage','fixed') DEFAULT 'percentage',
  `assistant_rate` double(10,2) DEFAULT '0.00',
  `assistant_type` enum('percentage','fixed') DEFAULT 'fixed',
  `secretary_rate` double(10,2) DEFAULT '0.00',
  `secretary_type` enum('percentage','fixed') DEFAULT 'fixed',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

/*Data for the table `procedures_qty_req_tbl` */

insert  into `procedures_qty_req_tbl`(`id`,`procedure_id`,`qty`,`dentist_rate`,`dentist_type`,`assistant_rate`,`assistant_type`,`secretary_rate`,`secretary_type`,`date_created`) values 
(31,66,1,20.00,'percentage',100.00,'fixed',0.00,'fixed','2024-05-28 05:08:40'),
(32,66,2,20.00,'percentage',100.00,'fixed',0.00,'fixed','2024-05-28 05:08:40'),
(33,66,3,20.00,'percentage',100.00,'fixed',0.00,'fixed','2024-05-28 05:08:40'),
(34,66,4,20.00,'percentage',100.00,'fixed',0.00,'fixed','2024-05-28 05:08:40'),
(35,66,5,20.00,'percentage',200.00,'fixed',200.00,'fixed','2024-05-28 05:08:40'),
(36,66,6,20.00,'percentage',200.00,'fixed',200.00,'fixed','2024-05-28 05:08:40'),
(37,66,7,20.00,'percentage',200.00,'fixed',200.00,'fixed','2024-05-28 05:08:40'),
(48,25,1,20.00,'percentage',100.00,'fixed',0.00,NULL,'2024-05-29 06:24:42'),
(49,25,2,20.00,'percentage',100.00,'fixed',0.00,NULL,'2024-05-29 06:24:42'),
(50,25,3,20.00,'percentage',100.00,'fixed',0.00,NULL,'2024-05-29 06:24:42'),
(51,25,4,20.00,'percentage',100.00,'fixed',0.00,NULL,'2024-05-29 06:24:42'),
(52,25,5,20.00,'percentage',200.00,'fixed',200.00,'fixed','2024-05-29 06:24:42'),
(53,49,1,100.00,'fixed',0.00,NULL,0.00,NULL,'2024-05-29 06:34:02'),
(54,49,2,200.00,'fixed',0.00,NULL,0.00,NULL,'2024-05-29 06:34:02'),
(55,49,3,300.00,'fixed',0.00,NULL,0.00,NULL,'2024-05-29 06:34:02'),
(56,49,4,400.00,'fixed',0.00,NULL,0.00,NULL,'2024-05-29 06:34:02');

/*Table structure for table `procedures_tbl` */

DROP TABLE IF EXISTS `procedures_tbl`;

CREATE TABLE `procedures_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `procedure_name` varchar(200) DEFAULT NULL,
  `procedure_desc` text,
  `price` double(10,2) DEFAULT NULL,
  `commission_type` enum('percentage','fixed') DEFAULT 'fixed',
  `commission_rate` int(11) DEFAULT NULL,
  `assistants_rate` int(11) DEFAULT '0',
  `assistants_type` enum('fixed','percentage') DEFAULT 'fixed',
  `secretary_rate` int(11) DEFAULT '0',
  `secretary_type` enum('fixed','percentage') DEFAULT 'fixed',
  `is_dentist_coms` tinyint(1) DEFAULT '1',
  `is_assist_coms` tinyint(1) DEFAULT '1',
  `is_secretary_coms` tinyint(1) DEFAULT '0',
  `isDentistAllowed` tinyint(1) DEFAULT '1',
  `isAssistantAllowed` tinyint(1) DEFAULT '1',
  `isSecretaryAllowed` tinyint(1) DEFAULT '1',
  `haveQuantity` tinyint(1) DEFAULT '0',
  `quantityComsThreshold` int(11) DEFAULT '0',
  `greaterThanQty` int(11) DEFAULT '0',
  `lessThanQty` int(11) DEFAULT '0',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

/*Data for the table `procedures_tbl` */

insert  into `procedures_tbl`(`id`,`parent_id`,`procedure_name`,`procedure_desc`,`price`,`commission_type`,`commission_rate`,`assistants_rate`,`assistants_type`,`secretary_rate`,`secretary_type`,`is_dentist_coms`,`is_assist_coms`,`is_secretary_coms`,`isDentistAllowed`,`isAssistantAllowed`,`isSecretaryAllowed`,`haveQuantity`,`quantityComsThreshold`,`greaterThanQty`,`lessThanQty`,`created_by`,`date_created`) values 
(1,0,'General Procedure',NULL,NULL,'fixed',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 17:53:23'),
(2,1,'Restoration(filling)',NULL,NULL,'percentage',10,20,'fixed',20,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 17:53:40'),
(3,1,'Oral Prophylaxis(cleaning)',NULL,NULL,'percentage',10,0,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 17:55:42'),
(4,1,'Tooth extraction',NULL,NULL,'percentage',10,0,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 17:55:55'),
(5,1,'Recementation of Crowns',NULL,NULL,'percentage',10,0,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 17:56:16'),
(6,1,'Removal of old braces with OP',NULL,NULL,'percentage',10,0,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 17:56:25'),
(7,1,'Ortho cement removal with OP',NULL,NULL,'percentage',10,0,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 17:56:27'),
(8,0,'Oral Surgery',NULL,NULL,'fixed',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 17:57:49'),
(9,8,'Wisdom Tooth Removal',NULL,NULL,'percentage',50,100,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 17:58:20'),
(10,8,'Gingivectomy',NULL,NULL,'percentage',50,100,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 17:58:35'),
(11,8,'Operculectomy',NULL,NULL,'percentage',50,100,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 17:58:44'),
(12,8,'Frenectomy',NULL,NULL,'percentage',50,100,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 17:58:55'),
(13,8,'Lip Repositioning',NULL,NULL,'percentage',50,100,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 17:59:06'),
(14,0,'Endodontic',NULL,NULL,'fixed',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:00:18'),
(15,14,'Anterior (front tooth)',NULL,NULL,'percentage',20,20,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 18:00:33'),
(16,14,'Premolars',NULL,NULL,'percentage',20,20,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:00:52'),
(17,14,'Molars',NULL,NULL,'percentage',20,20,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:01:22'),
(18,0,'Prosthodontic',NULL,NULL,'fixed',NULL,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:02:19'),
(19,18,'Dentures',NULL,NULL,'percentage',20,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:02:25'),
(20,18,'Fixed Bridge/Crowns',NULL,NULL,'percentage',20,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:03:05'),
(21,18,'Retainers',NULL,NULL,'percentage',20,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:04:26'),
(22,0,'Veneers',NULL,NULL,'fixed',NULL,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:04:37'),
(23,22,'Direct Veneers',NULL,NULL,'percentage',20,200,'fixed',0,'fixed',1,1,0,1,1,1,1,5,400,200,1,'2024-04-24 18:04:46'),
(24,22,'Indirect Veneers',NULL,NULL,'percentage',20,200,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:04:58'),
(25,22,'EMAX Veneers',NULL,NULL,'percentage',NULL,NULL,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-04-24 18:05:03'),
(26,22,'Zirconia Veneers',NULL,NULL,'percentage',20,200,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-04-24 18:06:52'),
(37,0,'Orthodontics','',NULL,'fixed',NULL,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:47:18'),
(38,37,'Metal Adjustment','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:48:01'),
(39,37,'SLB Adjustment','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:48:11'),
(40,37,'Ceramic Adjustment','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:48:23'),
(41,37,'New Metal Ortho','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:48:34'),
(42,37,'New Ceramic Ortho','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:48:58'),
(43,37,'New SLB Ortho','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:05'),
(44,37,'New SWLF Ortho','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:12'),
(45,37,'New Sapphire Ortho','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:19'),
(46,37,'New Invisalign Ortho','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:25'),
(47,37,'Debond Bracket','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:36'),
(48,37,'Debond Buccal Tube','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:42'),
(49,37,'Lost Bracket','',NULL,'fixed',NULL,NULL,'fixed',NULL,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:49'),
(50,37,'Lost Buccal Tube','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:49:59'),
(51,37,'Ortho Splint','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:50:06'),
(52,37,'Ortho Appliance','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:50:13'),
(53,37,'Premature Removal of Braces','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-16 03:50:22'),
(54,0,'Periodontal','',NULL,'fixed',NULL,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:30:35'),
(55,54,'New Perio','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:30:47'),
(56,54,'Follow-up Perio','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:31:09'),
(57,54,'Splint Perio','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:31:18'),
(58,0,'X-Ray','',NULL,'fixed',NULL,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:36:34'),
(59,58,'Panoramic X-Ray','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:38:06'),
(60,58,'Periapical X-Ray','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:38:18'),
(61,58,'CBCT (w/cd & usb)','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:42:12'),
(62,58,'CBCT (w/cd only)','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:42:21'),
(63,58,'TMJ X-Ray w/ Panoramic X-Ray','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:42:29'),
(64,58,'Panoramic X-Ray for Orthodontic Patients*','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-17 02:42:37'),
(65,58,'Reprint Xray','',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,0,0,0,0,0,0,0,1,'2024-05-17 17:06:49'),
(66,22,'Test Veneers','sdafdsafsd',NULL,'percentage',0,0,'fixed',0,'fixed',1,1,0,1,1,1,1,0,0,0,1,'2024-05-24 02:01:55'),
(67,0,'asdas','dasdas',NULL,'fixed',NULL,0,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-06-03 08:19:08'),
(68,0,'dfsaf','dsafsda',NULL,'fixed',NULL,0,'fixed',0,'fixed',1,1,0,1,1,1,0,0,0,0,1,'2024-06-03 08:20:04');

/*Table structure for table `rank_commission_benefits_mapping_tbl` */

DROP TABLE IF EXISTS `rank_commission_benefits_mapping_tbl`;

CREATE TABLE `rank_commission_benefits_mapping_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rank_id` bigint(20) NOT NULL,
  `procedure_id` bigint(20) NOT NULL,
  `commission_rate` int(11) DEFAULT NULL,
  `commission_type` enum('percentage','fixed') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `rank_commission_benefits_mapping_tbl` */

insert  into `rank_commission_benefits_mapping_tbl`(`id`,`rank_id`,`procedure_id`,`commission_rate`,`commission_type`) values 
(1,2,15,30,'percentage'),
(2,2,16,30,'percentage'),
(3,2,17,30,'percentage'),
(4,2,19,30,'percentage'),
(5,2,20,30,'percentage'),
(6,2,21,30,'percentage');

/*Table structure for table `ranks_tbl` */

DROP TABLE IF EXISTS `ranks_tbl`;

CREATE TABLE `ranks_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rank_name` varchar(100) DEFAULT NULL,
  `commission_rate` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ranks_tbl` */

insert  into `ranks_tbl`(`id`,`rank_name`,`commission_rate`,`date_created`) values 
(1,'Owner',NULL,'2024-04-24 18:23:39'),
(2,'Senior Dentist',NULL,'2024-04-24 18:23:43'),
(3,'Junior Dentist',NULL,'2024-04-29 00:30:25');

/*Table structure for table `roles_lookup_tbl` */

DROP TABLE IF EXISTS `roles_lookup_tbl`;

CREATE TABLE `roles_lookup_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `roles_lookup_tbl` */

insert  into `roles_lookup_tbl`(`id`,`role_name`,`is_active`,`date_created`) values 
(1,'Super Administrator','1','2024-02-18 18:50:50'),
(2,'Branch Head','1','2024-04-07 02:53:11'),
(3,'Front Desk','0','2024-04-07 02:53:21'),
(4,'Dentist','0','2024-04-07 02:53:22');

/*Table structure for table `secretaries_commissions_tbl` */

DROP TABLE IF EXISTS `secretaries_commissions_tbl`;

CREATE TABLE `secretaries_commissions_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) DEFAULT NULL,
  `assistant_id` bigint(20) DEFAULT NULL,
  `procedure_id` bigint(20) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `status` enum('done','pending') DEFAULT 'done',
  `covered_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `secretaries_commissions_tbl` */

insert  into `secretaries_commissions_tbl`(`id`,`transaction_id`,`assistant_id`,`procedure_id`,`amount`,`status`,`covered_date`,`date_created`) values 
(1,2,1,66,200.00,'done','2024-05-28 05:18:00','2024-05-28 05:18:41'),
(2,4,1,25,200.00,'done','2024-05-29 00:00:00','2024-05-29 06:28:37'),
(3,7,2,25,200.00,'done','2024-05-29 00:00:00','2024-05-29 06:37:53');

/*Table structure for table `secretaries_tbl` */

DROP TABLE IF EXISTS `secretaries_tbl`;

CREATE TABLE `secretaries_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `secretaries_tbl` */

insert  into `secretaries_tbl`(`id`,`branch_id`,`fname`,`mname`,`lname`,`is_active`,`created_by`,`date_created`) values 
(1,1,'a','a','a',0,1,'2024-05-06 03:17:20'),
(2,1,'b','b','b',1,1,'2024-05-06 04:58:38'),
(3,3,'Franchesca','','Nafarrete',1,1,'2024-05-31 03:33:37'),
(4,3,'Kohley Queen','','Tolores',1,1,'2024-05-31 03:33:52'),
(5,3,'Thena Rose','','Cellano',1,1,'2024-05-31 03:34:23'),
(6,2,'Yolden Grace','','Saraus',1,1,'2024-05-31 03:34:34'),
(7,2,'Irish Cyrine','','Ajero',1,1,'2024-05-31 03:34:45'),
(8,1,'Trixie','','Barrion',1,1,'2024-05-31 03:34:57'),
(9,1,'Precious Ann','','Maca',1,1,'2024-05-31 03:35:06'),
(10,1,'sdafdsa','','fdsafdsa',1,1,'2024-06-03 08:16:08');

/*Table structure for table `teeth_chart_tbl` */

DROP TABLE IF EXISTS `teeth_chart_tbl`;

CREATE TABLE `teeth_chart_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tooth_number` varchar(100) DEFAULT NULL,
  `tooth_part` enum('up','left','bottom','right','center','whole') DEFAULT NULL,
  `img` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `teeth_chart_tbl` */

insert  into `teeth_chart_tbl`(`id`,`tooth_number`,`tooth_part`,`img`) values 
(1,'18',NULL,NULL),
(2,'17',NULL,NULL),
(3,'16',NULL,NULL),
(4,'15',NULL,NULL),
(5,'14',NULL,NULL),
(6,'13',NULL,NULL),
(7,'12',NULL,NULL),
(8,'11',NULL,NULL),
(9,'22',NULL,NULL),
(10,'23',NULL,NULL),
(11,'24',NULL,NULL),
(12,'25',NULL,NULL),
(13,'26',NULL,NULL),
(14,'27',NULL,NULL),
(15,'28',NULL,NULL);

/*Table structure for table `transaction_details_tbl` */

DROP TABLE IF EXISTS `transaction_details_tbl`;

CREATE TABLE `transaction_details_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` bigint(20) NOT NULL,
  `patient_id` bigint(20) DEFAULT NULL,
  `procedure_id` bigint(20) NOT NULL,
  `dentist_id` bigint(20) NOT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `commission_distributed` double(10,2) DEFAULT NULL,
  `assistants_commission_distributed` double(10,2) DEFAULT '0.00',
  `secretary_commission_distributed` double(10,2) DEFAULT '0.00',
  `company_revenue` double(10,2) DEFAULT NULL,
  `proc_status` enum('done','pending','paid-pending') DEFAULT 'done',
  `covered_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `transaction_details_tbl` */

insert  into `transaction_details_tbl`(`id`,`transaction_id`,`patient_id`,`procedure_id`,`dentist_id`,`amount`,`qty`,`commission_distributed`,`assistants_commission_distributed`,`secretary_commission_distributed`,`company_revenue`,`proc_status`,`covered_date`) values 
(1,1,5,66,4,1000.00,4,800.00,100.00,0.00,3100.00,'done','2024-05-28 05:17:00'),
(2,2,6,66,7,1000.00,5,1000.00,200.00,200.00,3600.00,'done','2024-05-28 05:18:00'),
(3,3,11,25,8,125000.00,1,25000.00,100.00,0.00,99900.00,'done','2024-05-29 00:00:00'),
(4,4,10,25,1,25000.00,5,25000.00,200.00,200.00,99600.00,'done','2024-05-29 00:00:00'),
(5,5,4,25,6,25000.00,3,15000.00,100.00,0.00,59900.00,'done','2024-05-29 00:00:00'),
(6,6,6,49,4,1000.00,4,1600.00,0.00,0.00,2400.00,'done','2024-05-29 00:00:00'),
(7,7,11,25,2,25000.00,5,25000.00,200.00,200.00,99600.00,'done','2024-05-29 00:00:00'),
(8,8,5,2,4,1000.00,1,100.00,0.00,0.00,900.00,'done','2024-06-01 05:47:35'),
(9,9,2,2,4,1000.00,1,100.00,0.00,0.00,900.00,'pending','2024-06-06 06:05:00'),
(10,10,5,15,6,15000.00,1,4500.00,20.00,0.00,10480.00,'done','2024-06-02 11:38:00'),
(11,11,5,15,7,15000.00,2,9000.00,40.00,0.00,20960.00,'done','2024-06-02 00:00:00'),
(12,12,5,2,4,10000.00,1,1000.00,0.00,0.00,9000.00,'done','2024-06-03 08:29:00'),
(13,13,5,2,4,1000.00,1,100.00,0.00,0.00,900.00,'done','2024-06-03 08:30:00');

/*Table structure for table `transaction_status_tbl` */

DROP TABLE IF EXISTS `transaction_status_tbl`;

CREATE TABLE `transaction_status_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `transaction_status_tbl` */

/*Table structure for table `transactions_tbl` */

DROP TABLE IF EXISTS `transactions_tbl`;

CREATE TABLE `transactions_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) NOT NULL DEFAULT '1',
  `mop` varchar(100) NOT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `status` enum('DONE','VOIDED','PENDING') DEFAULT 'DONE',
  `created_by` bigint(20) DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `transactions_tbl` */

insert  into `transactions_tbl`(`id`,`branch_id`,`mop`,`amount`,`status`,`created_by`,`date_created`) values 
(1,1,'CASH',4000.00,'DONE',1,'2024-05-28 05:17:54'),
(2,1,'CASH',5000.00,'DONE',1,'2024-05-28 05:18:41'),
(3,1,'CASH',125000.00,'DONE',1,'2024-05-29 06:26:35'),
(4,1,'CASH',125000.00,'DONE',1,'2024-05-29 06:28:37'),
(5,2,'CASH',75000.00,'DONE',1,'2024-05-29 06:31:19'),
(6,3,'CASH',4000.00,'DONE',1,'2024-05-29 06:35:09'),
(7,3,'CASH',125000.00,'DONE',1,'2024-05-29 06:37:53'),
(8,1,'CASH',0.00,'DONE',1,'2024-06-01 01:50:35'),
(9,1,'CASH',0.00,'DONE',1,'2024-06-01 06:05:55'),
(10,1,'CASH',15000.00,'DONE',1,'2024-06-02 11:38:11'),
(11,1,'CASH',30000.00,'DONE',1,'2024-06-02 11:40:16'),
(12,1,'CASH',10000.00,'DONE',1,'2024-06-03 08:29:10'),
(13,1,'CASH',1000.00,'DONE',1,'2024-06-03 08:30:30');

/*Table structure for table `users_tbl` */

DROP TABLE IF EXISTS `users_tbl`;

CREATE TABLE `users_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `mname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `passhash` varchar(200) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `users_tbl` */

insert  into `users_tbl`(`id`,`role_id`,`branch_id`,`email`,`fname`,`mname`,`lname`,`username`,`passhash`,`is_active`,`date_created`) values 
(1,1,1,NULL,'admin','admin','admin','admin','$2b$10$.oiRReK26GRXOIYYfXkTK./WC/x5BFyEAR9Mpl.lVaXhLj3sJmM3C','1','2024-02-18 18:51:13'),
(2,2,1,'','Makati','','User','makati','$2b$10$fFXMgAOcOJUjrDfyRfLNUuwXzmmuiEHewBOCxz9KQvhBBV9ykHOWa','1','2024-06-02 11:26:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './uploads');
    },
    filename: function (req, file, callback) {
        let timestamp = Date.now()
        let fileName = file.originalname;
        let fileExt = fileName.substr(fileName.lastIndexOf('.') + 1, fileName.length)
        let fileGenerated = fileName = "MT" + timestamp.toString() + "." + fileExt

        callback(null, fileGenerated);
    }
});

var upload = multer({ storage: storage }).single('file')

exports.uploadFile = async function (req, res, next) {

    let result = await saveTolocal(req, res)

    res.status(200).send(result);
}

const saveTolocal = async function (req, res) {
    return new Promise((resolve, reject) => {
        upload(req, res, async function (err) {
            if (err) {
                console.log(err)
                let arr = {
                    statusCode: 500,
                    devMessage: 'error'
                }
                resolve(arr);
            } else {
                console.log("check")
                console.log(res)
                let arr = {
                    statusCode: 200,
                    devMessage: res.req.file.filename
                }
                resolve(arr);
            }
        });
    })
}
const moment = require("moment")
const bcrypt = require('bcrypt');
const saltRounds = 10;

const encryptStr = async function(str){
    return new Promise((resolve, reject) => {
        bcrypt.hash(str, saltRounds, function(err, hash) {
            if(err) {
                throw err
            }
            resolve(hash)
        });
    })
}

const checkHash = async function(str,pass){
    return new Promise((resolve, reject) => {
        bcrypt.compare(str, pass, async function(err, result) {
            if (err) {
                throw err
            }
            resolve(result)
        });
    })

}

const generateReportName = async function(str){
    var ms = moment().format("YYYYMMDDHHmmss")
    var msec = moment().millisecond()

    console.log(str+ms+msec)
    return str+ms
}

module.exports = {
    encryptStr,
    checkHash,
    generateReportName
}
const mysql   = require('mysql')
const config  = require('../../config/config')
const conn = mysql.createPool({
    host        : config.Mysql.Host,
    user        : config.Mysql.User,
    password    : config.Mysql.Password,
    database    : config.Mysql.Database,
    charset     : config.Mysql.Charset,
    multipleStatements : true
})
exports.getQuery = async function(sql, params){
    return new Promise((resolve, reject) => {
        conn.getConnection(function(err, connection) {
            connection.query(sql,params, function(err, results) {
                if (err) {
                    connection.release()
                    throw err;
                }
                resolve(results);
                connection.release()
            });
        })
    })
}
exports.executeQuery = async function(sql, params){
  return new Promise((resolve, reject) => {
    conn.getConnection(function(err, connection) {
        connection.query(sql,params, function(err, results) {
            if (err) {
                connection.release()
                throw err;
            }
            resolve(results);
            connection.release()
        });
    })
  })
}
exports.findFirst = async function(sql, params){
    return new Promise((resolve, reject) => {
        conn.getConnection(async function(err, connection) {
            connection.query("SET SESSION group_concat_max_len = 10000;",[],async function(err1, res1){
                if(res1){
                    connection.query(sql,params, async function(err, results) {
                        if (err) {
                            connection.release()
                            throw err;
                        }
                        if(results.length !=0){
                            resolve(results[0]);
                        }
                        else{
                            resolve({});
                        }
                        connection.release()
                    });
                }
            })
        })
    })
}
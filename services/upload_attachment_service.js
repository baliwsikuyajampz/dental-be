var multer = require('multer');
var moment = require("moment")
var fs = require('fs');

const upload_path = './_tmp_uploads'
const all_files_path  = './_file_uploads'


var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        console.log(req)
        callback(null, upload_path);
    },
    filename: function (req, file, callback) {
        var dir = moment().format('YYYYMMDDhhmmss');

        let timestamp = Date.now()
        let fileName = file.originalname;
        let fileExt = fileName.substr(fileName.lastIndexOf('.') + 1, fileName.length)
        let fileGenerated = fileName = "ATTACHMENT_"+dir + timestamp.toString() + "." + fileExt

        callback(null, fileGenerated);
    }
});

var upload = multer({ storage: storage }).single('file')

exports.uploadFile = async function (req, res, next) {
    let result = await saveTolocal(req, res)
    res.status(200).send(result);
}

const saveTolocal = async function (req, res) {
    return new Promise((resolve, reject) => {
        upload(req, res, async function (err) {
            if (err) {
                let arr = {
                    statusCode: 500,
                    devMessage: 'error'
                }
                resolve(arr);
            } else {
                let folderName = all_files_path + "/" + req.body.module +"/" + req.body.key
                if (!fs.existsSync(folderName)) {
                    fs.mkdirSync(folderName);
                }

                let oldPath = upload_path + "/" + res.req.file.filename
                let newPath = folderName + "/" + res.req.file.filename
                fs.rename(oldPath, newPath, function (err) {
                    if (err) throw err
                    console.log('Successfully renamed - AKA moved!')
                  })

                let arr = {
                    statusCode: 200,
                    devMessage: res.req.file.filename
                }
                resolve(arr);
            }
        });
    })
}
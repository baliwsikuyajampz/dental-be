var model = require("../models/transactions_model")

exports.getPendingTransactions = async function(req,res,next) {
    const result = await model.getPendingTransactions(req)
    const total = await model.getPendingTransactionTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.updatePendingTransaction = async function(req,res,next) {
    const result = await model.updatePendingTransaction(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.updateSchedule = async function(req,res,next) {
    const result = await model.updateSchedule(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getPendingPayments = async function(req,res,next) {
    const result = await model.getPendingPayments(req)
    const total = await model.getPendingPaymentsTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.addPayment = async function(req,res,next) {
    const result = await model.addPayment(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}





var doctors_mdl = require("../models/doctors_model")

exports.get = async function(req,res,next) {
    const result = await doctors_mdl.get(req)
    const total = await doctors_mdl.getDoctorsTotalCount(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.save = async function(req,res,next) {
    const result = await doctors_mdl.save(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getDoctorCommissionById = async function(req,res,next) {
    const result = await doctors_mdl.getDoctorCommissionById(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getDoctorDetailsById = async function(req,res,next) {
    const result = await doctors_mdl.getDoctorDetailsById(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}







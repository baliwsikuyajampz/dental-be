var model = require("../models/expenses_model")

exports.getCategories = async function(req,res,next) {
    let result = await model.getCategories(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.saveCategories = async function(req,res,next) {
    let result = await model.saveCategories(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getCategoriesList = async function(req,res,next) {
    let result = await model.getCategoriesList(req)
    let total  = await model.getCategoriesTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.get = async function(req,res,next) {
    let result = await model.get(req)
    let total  = await model.getTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.save = async function(req,res,next) {
    let result = await model.save(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.deleteRecord = async function(req,res,next) {
    let result = await model.deleteRecord(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}


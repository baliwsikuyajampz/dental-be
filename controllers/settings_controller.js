var settings_model = require("../models/settings_model")

exports.getTeethStructure = async function(req,res,next) {
    const result = await settings_model.getTeethStructure(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}





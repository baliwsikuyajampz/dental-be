var users_mdl = require("../models/users_model")

exports.getUsers = async function(req,res,next) {
    //check if payload exists
    if(!req.body.page){
        res.send('page required');
        return
    }
    else if (!req.body.rows){
        res.send('rows required');
        return 
    }
    
    const result = await users_mdl.getUsers(req)
    const total = await users_mdl.getUsersCount(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total : total
    })
}

exports.auth = async function(req,res,next) {
    //check if payload exists
    if(!req.body.username){
        res.send('username required');
        return
    }
    else if (!req.body.password){
        res.send('password required');
        return 
    }
    
    const result = await users_mdl.auth(req)
    
    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.changePass = async function(req,res,next) {
    //check if payload exists
    if(!req.body.userid){
        res.send('userid required');
        return
    }
    else if (!req.body.oldpass){
        res.send('password required');
        return 
    }else if(!req.body.newpass){
        res.send('old password required');
        return 
    }
   
    const result = await users_mdl.changePass(req)
    
    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.getRoles = async function(req,res,next) {
    const result = await users_mdl.getRoles(req)
    
    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.saveUser = async function(req,res,next) {
    const result = await users_mdl.saveUser(req)
    
    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.deleteUser = async function(req,res,next) {
    const result = await users_mdl.deleteUser(req)
    
    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.getUserByIdLogs = async function(req,res,next) {
    const result = await users_mdl.getUserByIdLogs(req)
    const total = await users_mdl.getUserByIdLogsCount(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total   : total
    })
}

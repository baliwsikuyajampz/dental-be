var sales_model = require("../models/sales_model")

exports.save = async function(req,res,next) {
    const result = await sales_model.save(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.get = async function(req,res,next) {
    const result = await sales_model.get(req)
    const total = await sales_model.getTotalSales(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total : total
    })
}

exports.voidTransaction = async function(req,res,next) {
    const result = await sales_model.voidTransaction(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getSales = async function(req,res,next) {
    const result = await sales_model.getSales(req)
    const total = await sales_model.getTotalSales(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total : total

    })
}




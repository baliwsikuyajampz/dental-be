var mdl = require("../models/dashboard_model")

exports.getDailySales = async function(req,res,next) {
    const result = await mdl.getDailySales(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getDailyExpenses = async function(req,res,next) {
    const result = await mdl.getDailyExpenses(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getMonthlySales = async function(req,res,next) {
    const result = await mdl.getMonthlySales(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getPendingClients = async function(req,res,next) {
    const result = await mdl.getPendingClients(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getPendingPatientsList = async function(req,res,next) {
    const result = await mdl.getPendingPatientsList(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}


exports.getTotalUnpaidAmount = async function(req,res,next) {
    const result = await mdl.getTotalUnpaidAmount(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getGrandTotal = async function(req,res,next) {
    const result = await mdl.getGrandTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}






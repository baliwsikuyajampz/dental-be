var model = require("../models/reports_model")

exports.getProfitAndLoss = async function(req,res,next) {
    const result = await model.getProfitAndLoss(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getCommissions = async function(req,res,next) {
    const result = await model.getCommissions(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.test = async function(req,res,next) {
    const result = await model.test(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getAssistantsCommissions = async function(req,res,next) {
    const result = await model.getAssistantsCommissions(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getSecretaryCommissions = async function(req,res,next) {
    const result = await model.getSecretaryCommissions(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getDailySales = async function(req,res,next) {
    const result = await model.getDailySales(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getMonthlySales = async function(req,res,next) {
    const result = await model.getMonthlySales(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getAnnualSales = async function(req,res,next) {
    const result = await model.getAnnualSales(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getCommissionsSearch = async function(req,res,next) {
    const result = await model.getCommissionsSearch(req)
    const total = await model.getCommissionsSearchTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.getAssistantsCommissionsSearch = async function(req,res,next) {
    const result = await model.getAssistantsCommissionsSearch(req)
    const total = await model.getAssistantsCommissionsSearchTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.getSecretaryCommissionsSearch = async function(req,res,next) {
    const result = await model.getSecretaryCommissionsSearch(req)
    const total = await model.getSecretaryCommissionsSearchTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.getDailySalesDetailsByPageChange = async function(req,res,next) {
    const result = await model.getDailySalesDetailsByPageChange(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getMonthlySalesDetailsByPageChange = async function(req,res,next) {
    const result = await model.getMonthlySalesDetailsByPageChange(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getAnnualSalesDetailsByPageChange = async function(req,res,next) {
    const result = await model.getAnnualSalesDetailsByPageChange(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}





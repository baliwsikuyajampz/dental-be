var mdl = require("../models/assistants_model")

exports.get = async function(req,res,next) {
    const result = await mdl.get(req)
    const total = await mdl.getTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.save = async function(req,res,next) {
    const result = await mdl.save(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}


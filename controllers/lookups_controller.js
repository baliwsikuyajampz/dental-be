var lookups_mdl = require("../models/lookups_model")

exports.getPatients = async function(req,res,next) {
    const result = await lookups_mdl.getPatients(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getDoctors = async function(req,res,next) {
    const result = await lookups_mdl.getDoctors(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getProcedures = async function(req,res,next) {
    const result = await lookups_mdl.getProcedures(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getAssistants = async function(req,res,next) {
    const result = await lookups_mdl.getAssistants(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getBranchLookup = async function(req,res,next) {
    const result = await lookups_mdl.getBranchLookup(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getProcedureParents = async function(req,res,next) {
    let result = await lookups_mdl.getProcedureParents(req)

    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.getSecretaries = async function(req,res,next) {
    const result = await lookups_mdl.getSecretaries(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getExpenseCategories = async function(req,res,next) {
    const result = await lookups_mdl.getExpenseCategories(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getProceduresRanks = async function(req,res,next) {
    const result = await lookups_mdl.getProceduresRanks(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getRanks = async function(req,res,next) {
    const result = await lookups_mdl.getRanks(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getModeOfPayments = async function(req,res,next) {
    const result = await lookups_mdl.getModeOfPayments(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}



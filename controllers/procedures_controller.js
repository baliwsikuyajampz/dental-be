var procedures_model = require("../models/procedures_model")

exports.get = async function(req,res,next) {
    let result = await procedures_model.get(req)
    let total = await procedures_model.getTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.save = async function(req,res,next) {
    let result = await procedures_model.save(req)

    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.categorySave = async function(req,res,next) {
    let result = await procedures_model.categorySave(req)

    res.json({
        statusCode : 200,
        devMessage : result
    })
}

exports.categoryGet = async function(req,res,next) {
    let result = await procedures_model.categoryGet(req)
    let total = await procedures_model.getTotalCategory(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}











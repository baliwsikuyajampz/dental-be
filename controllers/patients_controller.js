var patients_mdl = require("../models/patients_model")

exports.get = async function(req,res,next) {
    const result = await patients_mdl.get(req)
    const total = await patients_mdl.getTotal(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total      : total
    })
}

exports.save = async function(req,res,next) {
    const result = await patients_mdl.save(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getPatientById = async function(req,res,next) {
    const result = await patients_mdl.getPatientById(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getPatientInfoById = async function(req,res,next) {
    const result = await patients_mdl.getPatientInfoById(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.searchPatientByName = async function(req,res,next) {
    const result = await patients_mdl.searchPatientByName(req)
    const total = await patients_mdl.searchPatientByNameCount(req)

    res.json({
        statusCode : 200,
        devMessage : result,
        total : total
    })
}

exports.saveUploadAttachment = async function(req,res,next) {
    const result = await patients_mdl.saveUploadAttachment(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getUploadAttachment = async function(req,res,next) {
    const result = await patients_mdl.getUploadAttachment(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.saveUploadProfile = async function(req,res,next) {
    const result = await patients_mdl.saveUploadProfile(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.updatePatientAbout = async function(req,res,next) {
    const result = await patients_mdl.updatePatientAbout(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.getPatientTransactionsById = async function(req,res,next) {
    const result = await patients_mdl.getPatientTransactionsById(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}

exports.deletePatient = async function(req,res,next) {
    const result = await patients_mdl.deletePatient(req)

    res.json({
        statusCode : 200,
        devMessage : result,
    })
}



